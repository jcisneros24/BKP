package com.cmc.bkpclient.bean;

import java.io.InputStream;
import java.io.Serializable;

public class AttachmentBean implements Serializable {

	private static final long serialVersionUID = -8207282065566702465L;
	
    private InputStream is;
    private String nombre;
    private String tipo;
    
	public InputStream getIs() {
		return is;
	}
	public void setIs(InputStream is) {
		this.is = is;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}	
}