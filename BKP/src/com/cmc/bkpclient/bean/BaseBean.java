package com.cmc.bkpclient.bean;

public class BaseBean {
	
	private String key;
	private String value;
	private int id;
	private String respuesta;
	private String data;
	
	public BaseBean() {

	}
	
	public BaseBean(String respuesta, String data) {
		super();
		this.respuesta = respuesta;
		this.data = data;
	}

	public BaseBean(String key, String value, int id, String respuesta, String data) {
		this.key = key;
		this.value = value;
		this.id = id;
		this.respuesta = respuesta;
		this.data = data;
	}
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	
	@Override
	public String toString() {
		return value;
	}
	
}
