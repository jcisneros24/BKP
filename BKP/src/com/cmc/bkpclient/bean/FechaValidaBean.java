package com.cmc.bkpclient.bean;

public class FechaValidaBean {

	private String fecha;
	private String tiempo;

	public FechaValidaBean() {
		// TODO Auto-generated constructor stub
	}

	public FechaValidaBean(String fecha, String tiempo) {
		this.fecha = fecha;
		this.tiempo = tiempo;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getTiempo() {
		return tiempo;
	}

	public void setTiempo(String tiempo) {
		this.tiempo = tiempo;
	}

}
