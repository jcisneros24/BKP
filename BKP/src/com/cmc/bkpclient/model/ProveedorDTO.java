package com.cmc.bkpclient.model;

import org.apache.commons.lang.StringUtils;

import com.cmc.bkpclient.util.ValidacionesUtil;

public class ProveedorDTO {

	private String codCamp;
	private String fechaInic;
	private String fechaFinal;
	private String tipoDoc;
	private String nroDoc;
	private String codCliente;
	private String codTrata;
	private String codProv;
	private String codGestion;
	private String codContacto;
	private String codMotivoContacto;
	private String codSubmotivoContacto;
	private String tipoDatoContacto;
	private String descDatoContacto;
	private String nroIntento;
	private String codRpta;
	private String numLecturas;
	private String numRebotes;
	private String fechaHora;
	private String nuevo_email;

	public ProveedorDTO() {
	}
	
	public ProveedorDTO(String[] linea) {
		this.codCamp = linea[0];
		this.fechaInic = linea[1];
		this.fechaFinal = linea[2];
		this.tipoDoc = linea[3];
		this.nroDoc = linea[4];
		this.codCliente = linea[5];
		this.codTrata = linea[6];
		this.codProv = linea[7];
		this.codGestion = linea[8];
		this.codContacto = linea[9];
		this.codMotivoContacto = linea[10];
		this.codSubmotivoContacto = linea[11];
		this.tipoDatoContacto = linea[12];
		this.descDatoContacto = linea[13];
		this.nroIntento = linea[14];
		this.codRpta = linea[15];
		this.numLecturas = linea[16];
		this.numRebotes = linea[17];
		this.fechaHora = linea[18];
		this.nuevo_email = linea[19];
	}

	public String getCodCamp() {	
		return codCamp;
	}

	public void setCodCamp(String codCamp) {
			 this.codCamp = codCamp;
	}

	public String getFechaInic() {
		return fechaInic;
	}

	public void setFechaInic(String fechaInic) {
		this.fechaInic = fechaInic;
	}

	public String getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(String fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public String getTipoDoc() {
		return tipoDoc;
	}

	public void setTipoDoc(String tipoDoc) {
		this.tipoDoc = tipoDoc;
	}

	public String getNroDoc() {
		return nroDoc;
	}

	public void setNroDoc(String nroDoc) {
		this.nroDoc = nroDoc;
	}

	public String getCodCliente() {
		return codCliente;
	}

	public void setCodCliente(String codCliente) {
		this.codCliente = codCliente;
	}

	public String getCodTrata() {
		return codTrata;
	}

	public void setCodTrata(String codTrata) {
		this.codTrata = codTrata;
	}

	public String getCodProv() {
		return codProv;
	}

	public void setCodProv(String codProv) {
		this.codProv = codProv;
	}

	public String getCodGestion() {
		return codGestion;
	}

	public void setCodGestion(String codGestion) {
		this.codGestion = codGestion;
	}

	public String getCodContacto() {
		return codContacto;
	}

	public void setCodContacto(String codContacto) {
		this.codContacto = codContacto;
	}

	public String getCodMotivoContacto() {
		return codMotivoContacto;
	}

	public void setCodMotivoContacto(String codMotivoContacto) {
		this.codMotivoContacto = codMotivoContacto;
	}

	public String getCodSubmotivoContacto() {
		return codSubmotivoContacto;
	}

	public void setCodSubmotivoContacto(String codSubmotivoContacto) {
		this.codSubmotivoContacto = codSubmotivoContacto;
	}

	public String getTipoDatoContacto() {
		return tipoDatoContacto;
	}

	public void setTipoDatoContacto(String tipoDatoContacto) {
		this.tipoDatoContacto = tipoDatoContacto;
	}

	public String getDescDatoContacto() {
		return descDatoContacto;
	}

	public void setDescDatoContacto(String descDatoContacto) {
		this.descDatoContacto = descDatoContacto;
	}

	public String getNroIntento() {
		return nroIntento;
	}

	public void setNroIntento(String nroIntento) {
		this.nroIntento = nroIntento;
	}

	public String getCodRpta() {
		return codRpta;
	}

	public void setCodRpta(String codRpta) {
		this.codRpta = codRpta;
	}

	public String getNumLecturas() {
		return numLecturas;
	}

	public void setNumLecturas(String numLecturas) {
		this.numLecturas = numLecturas;
	}

	public String getNumRebotes() {
		return numRebotes;
	}

	public void setNumRebotes(String numRebotes) {
		this.numRebotes = numRebotes;
	}

	public String getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(String fechaHora) {
		this.fechaHora = fechaHora;
	}
	
	public String getNuevo_email() {
		return nuevo_email;
	}

	public void setNuevo_email(String nuevo_email) {
		this.nuevo_email = nuevo_email;
	}

	@Override
	public String toString() {
		
		return 
		  StringUtils.rightPad(ValidacionesUtil.validateLong(ValidacionesUtil.validateNullOrEmpty(codCamp),12),12, " ")
		+ StringUtils.leftPad(ValidacionesUtil.validateLong(ValidacionesUtil.validateNullOrEmpty(fechaInic),10), 10, " ")
		+ StringUtils.leftPad(ValidacionesUtil.validateLong(ValidacionesUtil.validateNullOrEmpty(fechaFinal),10), 10, " ")
		+ StringUtils.rightPad(ValidacionesUtil.validateLong(ValidacionesUtil.validateNullOrEmpty(tipoDoc),1), 1, " ") 
		+ StringUtils.rightPad(ValidacionesUtil.validateLong(ValidacionesUtil.validateNullOrEmpty(nroDoc),11), 11, " ")
		+ StringUtils.rightPad(ValidacionesUtil.validateLong(ValidacionesUtil.validateNullOrEmpty(codCliente),8), 8, " ")
		+ StringUtils.rightPad(ValidacionesUtil.validateLong(ValidacionesUtil.validateNullOrEmpty(codTrata),9), 9, " ")
		+ StringUtils.rightPad(ValidacionesUtil.validateLong(ValidacionesUtil.validateNullOrEmpty(codProv),7), 7, " ")
		+ StringUtils.rightPad(ValidacionesUtil.validateLong(ValidacionesUtil.validateNullOrEmpty(codGestion),4), 4, " ")
		+ StringUtils.rightPad(ValidacionesUtil.validateLong(ValidacionesUtil.validateNullOrEmpty(codContacto),4), 4, " ")
		+ StringUtils.rightPad(ValidacionesUtil.validateLong(ValidacionesUtil.validateNullOrEmpty(codMotivoContacto),4), 4, " ")
		+ StringUtils.rightPad(ValidacionesUtil.validateLong(ValidacionesUtil.validateNullOrEmpty(codSubmotivoContacto),4), 4, " ")
		+ StringUtils.rightPad(ValidacionesUtil.validateLong(ValidacionesUtil.validateNullOrEmpty(tipoDatoContacto),2), 2, " ")
		+ StringUtils.rightPad(ValidacionesUtil.validateLong(ValidacionesUtil.validateNullOrEmpty(descDatoContacto),250), 250, " ")
		+ StringUtils.leftPad(ValidacionesUtil.validateLong(ValidacionesUtil.validateNullOrEmpty(nroIntento),3), 3, "0")
		+ StringUtils.rightPad(ValidacionesUtil.validateLong(ValidacionesUtil.validateNullOrEmpty(codRpta),4), 4, " ")
		+ StringUtils.rightPad(ValidacionesUtil.validateLong(ValidacionesUtil.validateNullOrEmpty(numLecturas),10), 10, " ")
		+ StringUtils.rightPad(ValidacionesUtil.validateLong(ValidacionesUtil.validateNullOrEmpty(numRebotes),2), 2, " ")
		+ StringUtils.rightPad(ValidacionesUtil.validateLong(ValidacionesUtil.validateNullOrEmpty(fechaHora),24), 24, " ")
		+ StringUtils.rightPad(ValidacionesUtil.validateLong(ValidacionesUtil.validateNullOrEmpty(nuevo_email),100), 100, " ");
	}
	
}
