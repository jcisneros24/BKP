package com.cmc.bkpclient.model;

public class FolderDTO {

	private String destino;
	private String temporal;
	
	public FolderDTO() {

	}

	public FolderDTO(String destino, String temporal) {
		
		this.destino = destino;
		this.temporal = temporal;

	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public String getTemporal() {
		return temporal;
	}

	public void setTemporal(String temporal) {
		this.temporal = temporal;
	}

	@Override
	public String toString() {
		return "FolderBean [destino=" + destino + ", " +
							"temporal=" + temporal + "]";
	
	}

}
