package com.cmc.bkpclient.model;

public class CronDTO {
	
	private int numero;
	private String descripcion;
	private String meses;
	private String dia_semana;
	private String dias;
	private String horas;
	private String minutos;
	private String segundos;
	private String estado;
	private String expresion;
	
	public CronDTO() {
	}	

	public String getMeses() {
		return meses;
	}

	public void setMeses(String meses) {
		this.meses = meses;
	}

	public String getExpresion() {
		return expresion;
	}

	public void setExpresion(String expresion) {
		this.expresion = expresion;
	}

	
	public CronDTO(int numero, String descripcion, String meses,
			String dia_semana, String dias, String horas, String minutos,
			String segundos, String estado, String expresion) {
		this.numero = numero;
		this.descripcion = descripcion;
		this.dia_semana = dia_semana;
		this.dias = dias;
		this.horas = horas;
		this.minutos = minutos;
		this.segundos = segundos;
		this.estado = estado;
		this.expresion = expresion;
		this.meses = meses;
	}

	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getDia_semana() {
		return dia_semana;
	}
	public void setDia_semana(String dia_semana) {
		this.dia_semana = dia_semana;
	}
	public String getDias() {
		return dias;
	}
	public void setDias(String dias) {
		this.dias = dias;
	}
	public String getHoras() {
		return horas;
	}
	public void setHoras(String horas) {
		this.horas = horas;
	}
	public String getMinutos() {
		return minutos;
	}
	public void setMinutos(String minutos) {
		this.minutos = minutos;
	}
	public String getSegundos() {
		return segundos;
	}
	public void setSegundos(String segundos) {
		this.segundos = segundos;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	public String toString() {
		return  this.segundos + " " +
				this.minutos + " " +
				this.horas + " " +
				this.dias + " " +
				this.meses + " " +
				this.dia_semana;
	}
	
}
