package com.cmc.bkpclient.model;

import org.apache.commons.lang.StringUtils;

import com.cmc.bkpclient.util.ValidacionesUtil;

public class FileDTO {
	
	private String nombreArchivo;
	private String rutaArchivo;
	private int cantRegArchivo;
	private String fechaHoraProceso;
	
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	public String getRutaArchivo() {
		return rutaArchivo;
	}
	public void setRutaArchivo(String rutaArchivo) {
		this.rutaArchivo = rutaArchivo;
	}
	public int getCantRegArchivo() {
		return cantRegArchivo;
	}
	public void setCantRegArchivo(int cantRegArchivo) {
		this.cantRegArchivo = cantRegArchivo;
	}
	public String getFechaHoraProceso() {
		return fechaHoraProceso;
	}
	public void setFechaHoraProceso(String fechaHoraProceso) {
		this.fechaHoraProceso = fechaHoraProceso;
	}
	
	@Override
	public String toString() {
		return    
			  StringUtils.rightPad(ValidacionesUtil.validateLong(ValidacionesUtil.validateNullOrEmpty(nombreArchivo),38), 38, " ")
			+ StringUtils.rightPad(ValidacionesUtil.validateLong(ValidacionesUtil.validateNullOrEmpty(rutaArchivo),120), 120, " ")
			+ StringUtils.leftPad(ValidacionesUtil.validateLong(cantRegArchivo+"",8), 8, " ")
			+ StringUtils.leftPad(ValidacionesUtil.validateLong(ValidacionesUtil.validateNullOrEmpty(fechaHoraProceso),19), 19, " ");
	}
}
