package com.cmc.bkpclient.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.quartz.JobDetail;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cmc.bkpclient.bean.BaseBean;
import com.cmc.bkpclient.bean.UserBean;
import com.cmc.bkpclient.dao.PropertiesDaoImpl;
import com.cmc.bkpclient.job.scheduler.SchedulerMain;
import com.cmc.bkpclient.model.CronDTO;
import com.cmc.bkpclient.util.Constante;
import com.cmc.bkpclient.util.PropertiesUtil;
import com.cmc.bkpclient.util.RespuestaUtil;
import com.cmc.bkpclient.util.ValidacionesUtil;
import com.google.gson.Gson;

@RestController
@RequestMapping(value = "/methods")
public class RestApi {

	final static Logger logger = LoggerFactory.getLogger(RestApi.class);

	PropertiesDaoImpl propertiesDaoImpl = new PropertiesDaoImpl();
	PropertiesUtil propertiesUtil = new PropertiesUtil();
	Properties props = new Properties();
	List<BaseBean> lstPropsDTO = new ArrayList<BaseBean>();

	@RequestMapping(value = "/obtenerCRON", method = RequestMethod.GET, headers = "Accept=application/json")
	public String getCronDTO(HttpServletRequest request, HttpServletResponse response) throws IOException {

		List<CronDTO> listaCron = new ArrayList<CronDTO>();
		CronDTO cronDTO = propertiesDaoImpl.getCRON();
		listaCron.add(cronDTO);

		return new Gson().toJson(listaCron);

	}

	@RequestMapping(value = "/actualizarCron", method = RequestMethod.PUT, headers = "Accept=application/json")
	public String actualizarCron(@RequestBody CronDTO cron)
			throws SchedulerException, ClassNotFoundException, IOException {

		try {

			String[] cadena = cron.getExpresion().split(Constante.DESARROLLO.ESPACIO);
			cron.setSegundos("0");
			cron.setMinutos(cadena[0]);
			cron.setHoras(cadena[1]);

			cron.setMeses(cadena[3]);

			if (cadena[4].contains("*") || cadena[4].contains("SUN") || cadena[4].contains("MON")
					|| cadena[4].contains("TUE") || cadena[4].contains("WED") || cadena[4].contains("THU")
					|| cadena[4].contains("FRI") || cadena[4].contains("SAT")) {
				cron.setDias(cadena[2]);
				cadena[4] = "?";
				cron.setDia_semana(cadena[4]);

			} else {
				cron.setDias("?");
				/** Domingo **/
				if (cadena[4].equalsIgnoreCase("0")) {
					cadena[4] = "SUN";
					cron.setDia_semana(cadena[4]);
				} else {
					cron.setDia_semana(cadena[4]);
				}

				/** Lunes **/
				if (cadena[4].equalsIgnoreCase("1") || cadena[4] == "MON") {
					cadena[4] = "MON";
					cron.setDia_semana(cadena[4]);
				} else {
					cron.setDia_semana(cadena[4]);
				}

				/** Martes **/
				if (cadena[4].equalsIgnoreCase("2") || cadena[4] == "TUE") {
					cadena[4] = "TUE";
					cron.setDia_semana(cadena[4]);
				} else {
					cron.setDia_semana(cadena[4]);
				}

				/** Miercoles **/
				if (cadena[4].equalsIgnoreCase("3") || cadena[4] == "WED") {
					cadena[4] = "WED";
					cron.setDia_semana(cadena[4]);
				} else {
					cron.setDia_semana(cadena[4]);
				}

				/** Jueves **/
				if (cadena[4].equalsIgnoreCase("4") || cadena[4] == "THU") {
					cadena[4] = "THU";
					cron.setDia_semana(cadena[4]);
				} else {
					cron.setDia_semana(cadena[4]);
				}

				/** Viernes **/
				if (cadena[4].equalsIgnoreCase("5") || cadena[4] == "FRI") {
					cadena[4] = "FRI";
					cron.setDia_semana(cadena[4]);
				} else {
					cron.setDia_semana(cadena[4]);
				}

				/** Sabado **/
				if (cadena[4].equalsIgnoreCase("6") || cadena[4] == "SAT") {
					cadena[4] = "SAT";
					cron.setDia_semana(cadena[4]);
				} else {
					cron.setDia_semana(cadena[4]);
				}
			}

			logger.info("****" + " Inicia Carga de Archivo Properties " + "****");
			propertiesUtil.loadPropertiesFile();
			CronDTO cronAnterior = propertiesDaoImpl.getCRON();

			if (cronAnterior.getEstado().equals(Constante.ESTADO.ACTIVO)) {

				if (cron.getEstado().equals(Constante.ESTADO.ACTIVO)) {
					SchedulerMain.updateScheduler(cron);
				} else {
					SchedulerMain.deleteScheduler(cron);
				}

			} else {

				if (cron.getEstado().equals(Constante.ESTADO.ACTIVO)) {

					JobDetail jobDetail = SchedulerMain.initializeJobDetail(cron);
					Trigger trigger = SchedulerMain.initializeTrigger(cron);

					SchedulerMain.executeScheduler(jobDetail, trigger);
				}

			}

			propertiesDaoImpl.updateCRON(cron);

			return new Gson().toJson(
					new BaseBean(Constante.RESPUESTA_SERVICIOS.JOB_OK_HEAD, Constante.RESPUESTA_SERVICIOS.JOB_OK_BODY));
		} catch (IOException e) {
			logger.error("****" + "[Error en: {RestApi} - actualizarCron]" + e.getMessage() + "****");
			return new Gson().toJson(new BaseBean(Constante.RESPUESTA_SERVICIOS.ERROR_PROCESO,
					Constante.RESPUESTA_SERVICIOS.JOB_ERROR_BODY));
		}

	}

	@RequestMapping(value = "/obtenerLstPaths", method = RequestMethod.GET, headers = "Accept=application/json")
	public String getListPaths(HttpServletRequest request, HttpServletResponse response) throws IOException {
		logger.info("****" + " Inicia Carga de Archivo Properties " + "****");
		props = propertiesUtil.loadPropertiesFile();
		List<BaseBean> lstPropsDTO = new ArrayList<BaseBean>();

		int count = 0;

		for (String key : props.stringPropertyNames()) {
			String value = props.getProperty(key);

			if (!ValidacionesUtil.validarPropiedadObligatoria(key)) {
				count++;
				BaseBean propsDTO = new BaseBean();
				propsDTO.setKey(key);
				propsDTO.setValue(value);
				propsDTO.setId(count);
				lstPropsDTO.add(propsDTO);

			}

		}

		return new Gson().toJson(lstPropsDTO);
	}

	@RequestMapping(value = "/eliminarPropiedad", method = RequestMethod.DELETE, headers = "Accept=application/json")
	public String eliminarPropiedad(@RequestParam String llave) {

		try {
			propertiesDaoImpl.deletePropiedad(llave);
		} catch (IOException e) {
			logger.error("****" + "[Error en: {RestApi} - eliminarPropiedad]" + e.getMessage() + "****");
		}

		return new Gson().toJson(new BaseBean(Constante.RESPUESTA_SERVICIOS.RUTA_ELIMINADA_HEAD,
				Constante.RESPUESTA_SERVICIOS.RUTA_ELIMINADA_BODY));
	}

	@RequestMapping(value = "/guardarPropiedad", method = RequestMethod.POST, headers = "Accept=application/json")
	public String guardarPropiedad(@RequestBody BaseBean baseBean) {

		String resultado = null;
		boolean propLineExiste = false;

		try {

			logger.info("****" + " Validando datos de entrada... " + "****");

			if (baseBean.getKey() == null || baseBean.getValue() == null) {

				resultado = new Gson().toJson(new BaseBean(Constante.RESPUESTA_SERVICIOS.ERROR_PROCESO,
						Constante.RESPUESTA_SERVICIOS.RUTA_CAMPOS_VACIOS));

			} else {

				if (!baseBean.getKey().isEmpty() && !baseBean.getValue().isEmpty()) {

					if (baseBean.getKey().startsWith(Constante.CONFIGURACION.PREFIJO_RUTA_ENTRADA)
							&& baseBean.getKey().length() == 12) {

						if (Pattern.matches(Constante.REGEX.LINUX_PATH, baseBean.getValue())) {

							for (String key : props.stringPropertyNames()) {
								String value = props.getProperty(key);

								if (key.contains(baseBean.getKey())) {

									logger.info("****" + " No se pudo crear la carpeta. El codigo ingresado ya existe. "
											+ "****");
									resultado = new Gson()
											.toJson(new BaseBean(Constante.RESPUESTA_SERVICIOS.ERROR_PROCESO,
													Constante.RESPUESTA_SERVICIOS.RUTA_CODIGO_EXISTE));
									propLineExiste = true;
									break;

								}

								if (value.contains(baseBean.getValue())) {

									logger.info(
											"****" + " No se pudo crear la carpeta. La ruta de archivo ingresada ya existe. "
													+ "****");
									resultado = new Gson()
											.toJson(new BaseBean(Constante.RESPUESTA_SERVICIOS.ERROR_PROCESO,
													Constante.RESPUESTA_SERVICIOS.RUTA_CARPETA_EXISTE));
									propLineExiste = true;
									break;
								}
							}

							if (!propLineExiste) {
								logger.info("****" + " Creando carpeta para empresa. " + "****");
								propertiesDaoImpl.createPropiedad(baseBean.getKey(), baseBean.getValue());

								resultado = new Gson()
										.toJson(new BaseBean(Constante.RESPUESTA_SERVICIOS.RUTA_CREADA_HEAD,
												Constante.RESPUESTA_SERVICIOS.RUTA_CREADA_BODY));
							}

						} else {
							resultado = new Gson().toJson(new BaseBean(Constante.RESPUESTA_SERVICIOS.ERROR_PROCESO,
									Constante.RESPUESTA_SERVICIOS.RUTA_CARPETA_NO_VALIDO));
						}

					} else {
						resultado = new Gson().toJson(new BaseBean(Constante.RESPUESTA_SERVICIOS.ERROR_PROCESO,
								Constante.RESPUESTA_SERVICIOS.RUTA_CODIGO_NO_VALIDO));
					}

				} else {
					resultado = new Gson().toJson(new BaseBean(Constante.RESPUESTA_SERVICIOS.ERROR_PROCESO,
							Constante.RESPUESTA_SERVICIOS.RUTA_CAMPOS_VACIOS));
				}

			}

		} catch (IOException e) {
			logger.error("****" + "[Error en: {RestApi} - guardarPropiedad]" + e.getMessage() + "****");
		}

		return resultado;
	}

	@RequestMapping(value = "/actualizarPropiedad", method = RequestMethod.PUT, headers = "Accept=application/json")
	public String actualizarPropiedad(@RequestParam String llave, @RequestParam String valor) throws IOException {

		String resultado = null;
		boolean propLineExiste = false;

		try {

			if (llave == null || valor == null) {

				resultado = new Gson().toJson(new BaseBean(Constante.RESPUESTA_SERVICIOS.ERROR_PROCESO,
						Constante.RESPUESTA_SERVICIOS.RUTA_CAMPOS_VACIOS));

			} else {

				if (!llave.isEmpty() && !valor.isEmpty()) {

					if (Pattern.matches(Constante.REGEX.LINUX_PATH, valor)) {

						for (String key : props.stringPropertyNames()) {
							String value = props.getProperty(key);

							if (value.equalsIgnoreCase(valor)) {

								logger.info("****" + " No se pudo actualizar la carpeta. La ruta ingresada ya existe. "
										+ "****");
								resultado = new Gson().toJson(new BaseBean(Constante.RESPUESTA_SERVICIOS.ERROR_PROCESO,
										Constante.RESPUESTA_SERVICIOS.RUTA_CARPETA_EXISTE));
								propLineExiste = true;
								break;
							}
						}

						if (!propLineExiste) {

							logger.info("****" + " Actualizando propiedades de carpeta. " + "****");
							propertiesDaoImpl.updatePropiedad(llave, valor);

							resultado = new Gson()
									.toJson(new BaseBean(Constante.RESPUESTA_SERVICIOS.RUTA_ACTUALIZADA_HEAD,
											Constante.RESPUESTA_SERVICIOS.RUTA_ACTUALIZADA_BODY));
						}

					} else {
						resultado = new Gson().toJson(new BaseBean(Constante.RESPUESTA_SERVICIOS.ERROR_PROCESO,
								Constante.RESPUESTA_SERVICIOS.RUTA_CARPETA_NO_VALIDO));
					}

				} else {
					resultado = new Gson().toJson(new BaseBean(Constante.RESPUESTA_SERVICIOS.ERROR_PROCESO,
							Constante.RESPUESTA_SERVICIOS.RUTA_CAMPOS_VACIOS));
				}

			}

		} catch (IOException e) {
			logger.error("****" + "[Error en: {RestApi} - actualizarPropiedad]" + e.getMessage() + "****");
		}

		return resultado;
	}

	@RequestMapping(value = "/obtenerPath", method = RequestMethod.GET, headers = "Accept=application/json")
	public String getPath(@RequestParam String llave) throws IOException {

		BaseBean baseBean = new BaseBean();
		try {
			baseBean = propertiesDaoImpl.readPropiedad(llave);
		} catch (IOException e) {
			logger.error("****" + "[Error en: {RestApi} - obtenerPath]" + e.getMessage() + "****");
		}
		return new Gson().toJson(baseBean);
	}

	@RequestMapping(value = "/validarLogin", method = RequestMethod.PUT, headers = "Accept=application/json")
	public String validarLogin(@RequestBody UserBean user) {

		String response = null;

		try {
			if (user.getUser() != null && user.getPassword() != null) {

				String usuario = user.getUser();
				String password = user.getPassword();

				if (usuario.equals(Constante.PARAMETRO.ADMIN) && password.equals(Constante.PARAMETRO.PWD_ADMIN)) {
					response = new Gson().toJson(new RespuestaUtil(usuario, "Administrador"));

				} else {
					response = new Gson().toJson(new RespuestaUtil(Constante.RESPUESTA_SERVICIOS.ESTADO_ERROR,
							Constante.RESPUESTA_SERVICIOS.ESTADO_ERROR));
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			response = new Gson().toJson(
					new RespuestaUtil(Constante.RESPUESTA_SERVICIOS.ESTADO_ERROR, Constante.RESPUESTA_SERVICIOS.ERROR));
		}
		return response;
	}

}
