package com.cmc.bkpclient.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.github.junrar.extract.ExtractArchive;

import com.cmc.bkpclient.dao.PropertiesDaoImpl;
import com.cmc.bkpclient.model.FolderDTO;


public class RarUtils {
	
	private static final Logger logger = LogManager.getLogger(RarUtils.class);
	
	PropertiesDaoImpl PropertiesDaoImpl = new PropertiesDaoImpl();
	FolderDTO folderBean = new FolderDTO();
	
	String rutaAuxiliarRAR = null;
	
	public RarUtils() throws IOException {
		
		folderBean = PropertiesDaoImpl.getFolderPath();
		rutaAuxiliarRAR = folderBean.getTemporal();
	}

	public void extractFiles(File fileEntry){
		
		logger.info("**** " + "Inicia Extraccion de  Archivos RAR" + " ****");
		
		ExtractArchive extractArchive = new ExtractArchive();
		extractArchive.extractArchive(fileEntry, new File(rutaAuxiliarRAR));
		
	}
	
	public List<File> leerArchivosRAR(){
		
		logger.info("**** " + "Inicia Lectura de  Archivos RAR" + " ****");
		
		List<File> leerArchivosRAR = new ArrayList<File>();

		File directorioAuxiliar = new File(rutaAuxiliarRAR);
		File[] archivosDescomprimidos = directorioAuxiliar.listFiles();	
		
		for (File file : archivosDescomprimidos) {
			
				leerArchivosRAR.add(file);
			}
		
		return leerArchivosRAR;	
		
	}
	
}
