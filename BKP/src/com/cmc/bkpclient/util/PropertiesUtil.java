package com.cmc.bkpclient.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.vfs2.FileNotFolderException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.cmc.bkpclient.dao.PropertiesDaoImpl;

public class PropertiesUtil {

	private static final Logger logger = LogManager.getLogger(PropertiesUtil.class);

	public Properties loadPropertiesFile() throws FileNotFolderException, IOException {

		Properties propiedad = new Properties();
		InputStream input = null;
		boolean seCreoPFile = false;
		boolean seCreoDirectorio = false;

		try {

			String rutaPFile = Constante.PARAMETRO.DIRECCION_PROPERTIES;
			String archivoPFile = Constante.PARAMETRO.ARCHIVO_PROPERTIES;

			File pFile = new File(rutaPFile + archivoPFile);
			logger.info("**** " + "Validando Existencia de Archivo Properties..." + pFile.getAbsolutePath() + " ****");

			// Realiza carga cuando existe el archivo properties
			if (pFile.exists()) {

				logger.info("**** " + "Inicia carga de Archivo Properties Existente..." + " ****");

				input = new FileInputStream(rutaPFile + archivoPFile);
				propiedad.load(input);

				logger.info("**** " + "Termina Carga de Archivo Properties Existente." + " ****");

			} else {

				// Creamos Archivo Properties de no existir
				// Cuando no existe el directorio entonces creamos directorio y
				// archivo properties

				File directorio = new File(rutaPFile);

				if (!directorio.exists()) {

					logger.info("**** " + "Validando creacion de Directorio de Archivo properties." + " ****");
					seCreoDirectorio = ValidacionesUtil.crearDirectorio(directorio);

					if (seCreoDirectorio) {

						logger.info("**** " + "Validando creacion de Archivo properties." + " ****");
						seCreoPFile = ValidacionesUtil.crearProperties(pFile);

						if (seCreoPFile) {

							logger.info("****" + " Inicia carga de Archivo Properties Creado... " + "****");

							input = new FileInputStream(
									Constante.PARAMETRO.DIRECCION_PROPERTIES + Constante.PARAMETRO.ARCHIVO_PROPERTIES);
							propiedad.load(input);

							logger.info("****" + " Termina Carga de Archivo Properties Creado " + "****");

						} else {

							logger.info("**** " + Constante.ERROR_FUNCIONAL.EF_CREACION_ARCHIVO_PROPERTIES + " ****");
						}

					} else {

						logger.info("**** " + Constante.ERROR_FUNCIONAL.EF_CREACION_DIRECTORIO + " ****");
					}

				} else {

					// Cuando existe el directorio entonces creamos archivo
					// properties

					logger.info("**** " + "Creando Archivo properties." + " ****");
					seCreoPFile = ValidacionesUtil.crearProperties(pFile);

					if (seCreoPFile) {

						logger.info("**** " + "Inicia carga de Archivo Properties Creado..." + " ****");
						input = new FileInputStream(rutaPFile + archivoPFile);
						propiedad.load(input);

						logger.info("**** " + "Termina Carga de Archivo Properties Creado" + " ****");

					} else {

						logger.info("**** " + Constante.ERROR_FUNCIONAL.EF_CREACION_ARCHIVO_PROPERTIES + " ****");
					}

				}

			}

		} catch (FileNotFoundException ex) {
			logger.error("****" + "{Error en: [Metodo - loadPropertiesFile]}" + ex.getMessage() + "****");
		} catch (IOException e) {
			logger.error("****" + "{Error en: [Metodo - loadPropertiesFile]}" + e.getMessage() + "****");
		}
		return propiedad;
	}

	public void loadFolderRequired() {

		PropertiesDaoImpl propertiesDaoImpl = new PropertiesDaoImpl();

		String temporal = propertiesDaoImpl.getFolderPath().getTemporal();
		String destino = propertiesDaoImpl.getFolderPath().getDestino();

		/** Creacion de directorios obligatorios **/

		// Carpeta temporal
		ValidacionesUtil.crearDirectorio(new File(temporal));

		// Carpeta destino
		ValidacionesUtil.crearDirectorio(new File(destino));
	}

}
