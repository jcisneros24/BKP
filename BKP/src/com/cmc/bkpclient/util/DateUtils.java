package com.cmc.bkpclient.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.cmc.bkpclient.bean.FechaValidaBean;

public class DateUtils {

	private static final Logger logger = LogManager.getLogger(DateUtils.class);

	public static FechaValidaBean getFechaCreacion(String path) {

		logger.info("*** ruta: " + path);
		logger.info("***" + " Inicio de Validación de Fecha " + "***");

		try {

			String rutaEjecucion = "cmd /c dir \"" + path + "\" /tc";
			Process proc = Runtime.getRuntime().exec(rutaEjecucion);
			BufferedReader br = new BufferedReader(new InputStreamReader(proc.getInputStream()));

			String data = "";

			for (int i = 0; i < 6; i++) {
				data = br.readLine();
			}

			StringTokenizer st = new StringTokenizer(data);
			String date = st.nextToken();// Get date
			String time = st.nextToken();// Get time
			String ampm = st.nextToken();

			if (ampm.equals("a.m."))
				ampm = "AM";
			else if (ampm.equals("p.m."))
				ampm = "PM";

			time = time + " " + ampm;

			SimpleDateFormat date12Format = new SimpleDateFormat("hh:mm a");
			SimpleDateFormat date24Format = new SimpleDateFormat("HH:mm");
			try {
				time = date24Format.format(date12Format.parse(time));
			} catch (ParseException e) {
				e.printStackTrace();
			}

			FechaValidaBean fv = new FechaValidaBean();

			fv.setFecha(date);
			fv.setTiempo(time);

			return fv;

		} catch (IOException e) {

			e.printStackTrace();

		}

		return null;
	}

	public static String getDateFormat() {
		SimpleDateFormat sdf = new SimpleDateFormat(Constante.PARAMETRO.FORMATO_FECHA);
		Date now = new Date();
		String fecha = sdf.format(now);
		logger.info("***" + " Fecha Hoy (AAMMDD): " + fecha + "***");
		return fecha;
	}

}
