package com.cmc.bkpclient.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.cmc.bkpclient.model.FileDTO;
import com.cmc.bkpclient.model.ProveedorDTO;

public class TxtUtils {

	private static final Logger logger = LogManager.getLogger(TxtUtils.class);

	public static void filesGeneration(List<File> archivos, String rutaXCOM, File folder) throws IOException {

		if (!archivos.isEmpty() || archivos.size() > 0) {
			logger.info(
					"**** " + "Inicia Lectura y generaci�n de Archivos: Consolidado y Detalle del Proceso" + " ****");
			List<ProveedorDTO> lstTramaProveedor = new ArrayList<ProveedorDTO>();
			lstTramaProveedor = readFilesTramas(archivos);
			generateFileConsolidate(rutaXCOM, lstTramaProveedor);
			generateFileDetProc(rutaXCOM, archivos, folder);
		} else {
			logger.info("**** "+Constante.ERROR_FUNCIONAL.EF_LISTA_ARCHIVOS_VACIA+ " ****");	
			logger.info(
					"**** " + "Inicia generaci�n de Archivos por default: Consolidado y Detalle del Proceso" + " ****");
			generateFilesDefault(rutaXCOM);
		}
	}

	public static List<ProveedorDTO> readFilesTramas(List<File> archivos) {

		logger.info("**** " + "Inicia Lectura de Archivos por trama" + " ****");

		List<ProveedorDTO> lstTramaProveedor = new ArrayList<ProveedorDTO>();

		for (File file : archivos) {

			logger.info("**** " + "Archivos leidos: " + file.getName() + " ****");
			BufferedReader br = null;
			FileReader fr = null;
			long lineasValidas = 0;
			long lineasNoValidas = 0;
			logger.info("**** " + "Inicia validacion de Lista de archivos con extensiones permitidas" + " ****");
			String extension = FilenameUtils.getExtension(file.getName());
			logger.info("****" + " EXTENSION: " + extension + "****");

			if (extension.equalsIgnoreCase(Constante.EXTENSIONES.ZIP)
					|| extension.equalsIgnoreCase(Constante.EXTENSIONES.RAR)
					|| extension.equalsIgnoreCase(Constante.EXTENSIONES.TXT)) {

				try {
					fr = new FileReader(file);
					br = new BufferedReader(fr);
					String sCurrentLine;
					br = new BufferedReader(new FileReader(file));

					while ((sCurrentLine = br.readLine()) != null) {

						String lineaActual = sCurrentLine;
						lineaActual = lineaActual + Constante.CARACTERES.VACIO;
						String[] linea = lineaActual.split(Constante.CARACTERES.PIPELINE);

						if (linea.length == Constante.PARAMETRO.TAMA�O_COLUMNA_REPORTE) {

							lineasValidas++;
							if (lineasValidas >= 2) {

								ProveedorDTO proveedorDTO = new ProveedorDTO(linea);
								if (proveedorDTO.toString().length() == Constante.PARAMETRO.TAMA�O_LINEA_REPORTE) {
									lineasValidas++;
									lstTramaProveedor.add(proveedorDTO);

								} else {
									lineasNoValidas++;
									logger.info("**** " + "La cantidad de caracteres en linea: " + lineasNoValidas
											+ " no es valida." + " ****");
								}

							}
						}
					}
				} catch (IOException e) {
					logger.info("****" + "{Error en: [Metodo - lecturaArchivoTramas]}" + e.getMessage() + "****");

				} finally {

					try {

						if (br != null)
							br.close();

						if (fr != null)
							fr.close();

					} catch (IOException ex) {
						logger.error("****" + "{Error en: [Metodo - lecturaArchivoTramas]}" + ex.getMessage() + "****");
					}
				}

			} else {
				logger.info(
						"**** " + Constante.ERROR_FUNCIONAL.EF_NOMBRE_ESTANDAR_EXTENSION + file.getName() + " ****");
			}
		}
		return lstTramaProveedor;
	}

	public static void generateFileConsolidate(String rutaFinal, List<ProveedorDTO> tramas) throws IOException {

		logger.info("**** " + "Generacion Archivo Consolidado de FeedBack " + " ****");
		Date fechaSistema = new Date();
		SimpleDateFormat dt1 = new SimpleDateFormat("yyyyMMdd");
		String fechaArchivo = dt1.format(fechaSistema);

		File pDir = new File(rutaFinal);
		logger.info("**** " + "Ruta Destino de Archivo Consolidado: " + pDir.getName() + " ****");

		boolean seCreoDirectorio = false;

		try {

			File archivoSalida = new File(rutaFinal + File.separator + Constante.PARAMETRO.NOMBRE_ARCHIVO_UNIFICADO
					+ Constante.CARACTERES.GUION_BAJO + fechaArchivo + "." + Constante.EXTENSIONES.TXT);

			logger.info("**** " + "Comprobando Existencia: Ruta Destino de Archivo Consolidado" + " ****");

			if (archivoSalida.exists()) {

				if (tramas != null && tramas.size() > 0) {
					logger.info("**** " + "Agregando data a Archivo Consolidado existente." + " ****");
					for (ProveedorDTO proveedorDTO : tramas) {
						FileWriter fileWriter = new FileWriter(archivoSalida, true);
						fileWriter.write(proveedorDTO.toString());
						fileWriter.write(System.getProperty(Constante.CARACTERES.SEPARADOR_SISTEMA));
						fileWriter.close();
					}
					logger.info("**** " + "Termina generaci�n de Archivo Consolidado." + " ****");
				} else {
					logger.info("**** " + "Generando Archivo Consolidado vac�o." + " ****");
					FileWriter fileWriter = new FileWriter(archivoSalida,true);
					fileWriter.write(StringUtils.leftPad("", 479));
					fileWriter.write(System.getProperty(Constante.CARACTERES.SEPARADOR_SISTEMA));
					fileWriter.close();
					logger.info("**** " + "Termina generaci�n de Archivo Consolidado vac�o." + " ****");
				}

			} else {

				if (!pDir.exists()) {

					logger.info("**** " + "Validando creacion de Directorio para Archivo Consolidado." + " ****");

					seCreoDirectorio = ValidacionesUtil.crearDirectorio(pDir);
					logger.info("**** " + "Directorio para Archivo Consolidado creado: " + seCreoDirectorio);

					if (seCreoDirectorio) {

						if (tramas != null && tramas.size() > 0) {
							logger.info("**** " + "Agregando data a Archivo Consolidado creado." + " ****");
							for (ProveedorDTO proveedorDTO : tramas) {
								PrintWriter out = new PrintWriter(new FileWriter(archivoSalida, true));
								out.append(proveedorDTO.toString());
								out.append(System.getProperty(Constante.CARACTERES.SEPARADOR_SISTEMA));
								out.close();
							}
							logger.info("**** " + "Termina generaci�n de Archivo Consolidado." + " ****");
						} else {
							logger.info("**** " + "Generando Archivo Consolidado vac�o." + " ****");
							PrintWriter out = new PrintWriter(new FileWriter(archivoSalida,true));
							out.append(StringUtils.leftPad("", 479));
							out.append(System.getProperty(Constante.CARACTERES.SEPARADOR_SISTEMA));
							out.close();
							logger.info("**** " + "Termina generaci�n de Archivo Consolidado vac�o." + " ****");
						}

					} else {

						logger.info("**** " + Constante.ERROR_FUNCIONAL.EF_CREACION_DIRECTORIO + " ****");
					}

				} else {
					if (tramas != null && tramas.size() > 0) {
						logger.info("**** " + "Agregando data para Archivo Consolidado creado." + " ****");
						for (ProveedorDTO proveedorDTO : tramas) {
							PrintWriter out = new PrintWriter(new FileWriter(archivoSalida, true));
							out.append(proveedorDTO.toString());
							out.append(System.getProperty(Constante.CARACTERES.SEPARADOR_SISTEMA));
							out.close();
						}
						logger.info("**** " + "Termina generaci�n de Archivo Consolidado." + " ****");
					} else {
						logger.info("**** " + "Generando Archivo Consolidado vac�o." + " ****");
						PrintWriter out = new PrintWriter(new FileWriter(archivoSalida,true));
						out.append(StringUtils.leftPad("", 479));
						out.append(System.getProperty(Constante.CARACTERES.SEPARADOR_SISTEMA));
						out.close();
						logger.info("**** " + "Termina generaci�n de Archivo Consolidado vac�o." + " ****");
					}

				}

			}

		} catch (IOException e) {
			logger.error("****" + "{Error en: [Metodo - generateFileConsolidate]}" + e.getMessage() + "****");
		}

	}

	public static void generateFileDetProc(String rutaFinal, List<File> fileArchivos, File folder) throws IOException {

		logger.info("**** " + "Generacion Archivo Detalle del Proceso" + " ****");

		Date fechaSistema = new Date();

		SimpleDateFormat dt1 = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat dt2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		String fechaHoraProceso = dt2.format(fechaSistema);
		String fechaArchivo = dt1.format(fechaSistema);

		File pDir = new File(rutaFinal);
		logger.info("**** " + "Ruta Destino de Archivo Detalle del Proceso: " + pDir.getName() + " ****");

		boolean seCreoDirectorio = false;

		try {

			File archivoSalida = new File(
					rutaFinal + File.separator + Constante.PARAMETRO.NOMBRE_ARCHIVO_UNIFICADO_DETPROC
							+ Constante.CARACTERES.GUION_BAJO + fechaArchivo + "." + Constante.EXTENSIONES.TXT);

			logger.info("**** " + "Comprobando Existencia: Ruta Destino de Archivo Detalle del Proceso" + " ****");

			if (archivoSalida.exists()) {

				for (File file : fileArchivos) {

					logger.info(
							"**** " + "Inicia validacion de Lista de archivos con extensiones permitidas" + " ****");
					String extension = FilenameUtils.getExtension(file.getName());
					logger.info("****" + " EXTENSION: " + extension + "****");

					logger.info("**** " + "Inicia Lectura de Archivos para Generar Detalle del Proceso" + " ****");
					FileReader fileReader = new FileReader(file);

					BufferedReader br = new BufferedReader(fileReader);
					FileDTO line = new FileDTO();
					String sCurrentLine;
					int cabecera = 0;
					int lineasValidas = 0;

					while ((sCurrentLine = br.readLine()) != null) {

						String lineaActual = sCurrentLine;
						lineaActual = lineaActual + Constante.CARACTERES.VACIO;
						String[] linea = lineaActual.split(Constante.CARACTERES.PIPELINE);

						if (linea.length == Constante.PARAMETRO.TAMA�O_COLUMNA_REPORTE) {

							cabecera++;
							if (cabecera >= 2) {

								ProveedorDTO proveedorDTO = new ProveedorDTO(linea);
								if (proveedorDTO.toString().length() == Constante.PARAMETRO.TAMA�O_LINEA_REPORTE) {

									lineasValidas++;

								}
							}
						}
					}

					br.close();
					// wuc - validacion tama�o nombre del archivo
					if (file.getName().length() == Constante.PARAMETRO.TAMA�O_NOMBRE_PRE_TXT) {
						String sCadenaDT = "";
						String sSubCadenaDT = "";
						sCadenaDT = file.getName();
						sSubCadenaDT = sCadenaDT.substring(8, 46);
						logger.info("**TxtUtils**sSubCadenaDT " + sSubCadenaDT + " ****");
						line.setNombreArchivo(sSubCadenaDT);
					} else {
						line.setNombreArchivo(file.getName());
					}

					line.setRutaArchivo(folder.getAbsolutePath());
					line.setCantRegArchivo(lineasValidas);
					line.setFechaHoraProceso(fechaHoraProceso);
					logger.info("**** " + "Inicia Escritura de Archivos para Generar Detalle del Proceso" + " ****");
					logger.info(line);

					FileWriter fileWriter = new FileWriter(archivoSalida, true);
					fileWriter.write(line.toString());
					fileWriter.write(System.getProperty(Constante.CARACTERES.SEPARADOR_SISTEMA));
					fileWriter.close();

				}
				logger.info("**** " + "Termina generaci�n de Archivo Detalle del Proceso." + " ****");
			} else {

				if (!pDir.exists()) {

					logger.info(
							"**** " + "Validando creacion de Directorio para Archivo Detalle del Proceso." + " ****");
					seCreoDirectorio = ValidacionesUtil.crearDirectorio(pDir);

					if (seCreoDirectorio) {

						for (File file : fileArchivos) {
							logger.info("**** " + "Inicia validacion de Lista de archivos con extensiones permitidas"
									+ " ****");
							String extension = FilenameUtils.getExtension(file.getName());
							logger.info("****" + " EXTENSION: " + extension + "****");

							logger.info(
									"**** " + "Inicia Lectura de Archivos para Generar Detalle del Proceso" + " ****");
							FileReader fileReader = new FileReader(file);

							BufferedReader br = new BufferedReader(fileReader);
							FileDTO line = new FileDTO();
							String sCurrentLine;
							int cabecera = 0;
							int lineasValidas = 0;

							while ((sCurrentLine = br.readLine()) != null) {

								String lineaActual = sCurrentLine;
								lineaActual = lineaActual + Constante.CARACTERES.VACIO;
								String[] linea = lineaActual.split(Constante.CARACTERES.PIPELINE);

								if (linea.length == Constante.PARAMETRO.TAMA�O_COLUMNA_REPORTE) {

									cabecera++;
									if (cabecera >= 2) {

										ProveedorDTO proveedorDTO = new ProveedorDTO(linea);
										if (proveedorDTO.toString()
												.length() == Constante.PARAMETRO.TAMA�O_LINEA_REPORTE) {

											lineasValidas++;

										}
									}
								}
							}

							br.close();
							// wuc - validacion tama�o nombre del archivo
							if (file.getName().length() == Constante.PARAMETRO.TAMA�O_NOMBRE_PRE_TXT) {
								String sCadenaDT = "";
								String sSubCadenaDT = "";
								sCadenaDT = file.getName();
								sSubCadenaDT = sCadenaDT.substring(8, 46);
								logger.info("**TxtUtils**sSubCadenaDT " + sSubCadenaDT + " ****");
								line.setNombreArchivo(sSubCadenaDT);
							} else {
								line.setNombreArchivo(file.getName());
							}

							line.setRutaArchivo(folder.getAbsolutePath());
							line.setCantRegArchivo(lineasValidas);
							line.setFechaHoraProceso(fechaHoraProceso.toString());
							logger.info("**** " + "Inicia Escritura de Archivos para Generar Detalle del Proceso"
									+ " ****");
							logger.info(line);

							PrintWriter out = new PrintWriter(new FileWriter(archivoSalida, true));
							out.append(line.toString());
							out.append(System.getProperty(Constante.CARACTERES.SEPARADOR_SISTEMA));
							out.close();

						}

					} else {
						logger.info("**** " + Constante.ERROR_FUNCIONAL.EF_CREACION_DIRECTORIO + " ****");
					}
					logger.info("**** " + "Termina generaci�n de Archivo Detalle del Proceso." + " ****");
				} else {

					for (File file : fileArchivos) {

						logger.info("**** " + "Inicia validacion de Lista de archivos con extensiones permitidas"
								+ " ****");
						String extension = FilenameUtils.getExtension(file.getName());
						logger.info("****" + " EXTENSION: " + extension + "****");

						logger.info("**** " + "Inicia Lectura de Archivos para Generar Detalle del Proceso" + " ****");
						FileReader fileReader = new FileReader(file);

						BufferedReader br = new BufferedReader(fileReader);
						FileDTO line = new FileDTO();
						String sCurrentLine;
						int cabecera = 0;
						int lineasValidas = 0;

						while ((sCurrentLine = br.readLine()) != null) {

							String lineaActual = sCurrentLine;
							lineaActual = sCurrentLine + Constante.CARACTERES.VACIO;
							String[] linea = lineaActual.split(Constante.CARACTERES.PIPELINE);

							if (linea.length == Constante.PARAMETRO.TAMA�O_COLUMNA_REPORTE) {

								cabecera++;
								if (cabecera >= 2) {

									ProveedorDTO proveedorDTO = new ProveedorDTO(linea);
									if (proveedorDTO.toString().length() == Constante.PARAMETRO.TAMA�O_LINEA_REPORTE) {

										lineasValidas++;

									}
								}
							}
						}

						br.close();
						// wuc - validacion tama�o nombre del archivo
						if (file.getName().length() == Constante.PARAMETRO.TAMA�O_NOMBRE_PRE_TXT) {
							String sCadenaDT = "";
							String sSubCadenaDT = "";
							sCadenaDT = file.getName();
							sSubCadenaDT = sCadenaDT.substring(8, 46);
							logger.info("**TxtUtils**sSubCadenaDT " + sSubCadenaDT + " ****");
							line.setNombreArchivo(sSubCadenaDT);
						} else {
							line.setNombreArchivo(file.getName());
						}

						line.setRutaArchivo(folder.getAbsolutePath());
						line.setCantRegArchivo(lineasValidas);
						line.setFechaHoraProceso(fechaHoraProceso.toString());
						logger.info(
								"**** " + "Inicia Escritura de Archivos para Generar Detalle del Proceso" + " ****");
						logger.info(line);

						PrintWriter out = new PrintWriter(new FileWriter(archivoSalida, true));
						out.append(line.toString());
						out.append(System.getProperty(Constante.CARACTERES.SEPARADOR_SISTEMA));
						out.close();

					}
					logger.info("**** " + "Termina generaci�n de Archivo Detalle del Proceso." + " ****");
				}

			}

		} catch (IOException e) {
			logger.error("****" + "{Error en: [Metodo - generateFileDetProc]}" + e.getMessage() + "****");
		}

	}

	public static void generateFilesDefault(String rutaFinal) throws IOException {
		logger.info("**** " + "Generacion de Archivos Consolidado y Detalle del Proceso por defecto" + " ****");
		Date fechaSistema = new Date();
		SimpleDateFormat dt1 = new SimpleDateFormat("yyyyMMdd");
		String fechaArchivo = dt1.format(fechaSistema);

		File pDir = new File(rutaFinal);
		logger.info("**** " + "Ruta Destino de Archivos Consolidado y Detalle del Proceso por defecto: "
				+ pDir.getName() + " ****");

		try {

			File archivoSalidaConsolidado = new File(
					rutaFinal + File.separator + Constante.PARAMETRO.NOMBRE_ARCHIVO_UNIFICADO
							+ Constante.CARACTERES.GUION_BAJO + fechaArchivo + "." + Constante.EXTENSIONES.TXT);

			logger.info("**** " + "Comprobando Existencia: Ruta Destino de Archivo Consolidado" + " ****");

			logger.info("**** " + "Generando Archivo Consolidado vac�o." + " ****");
			FileWriter fwConsolidado = new FileWriter(archivoSalidaConsolidado, true);
			fwConsolidado.write(StringUtils.leftPad("", 479));
			fwConsolidado.write(System.getProperty(Constante.CARACTERES.SEPARADOR_SISTEMA));
			fwConsolidado.close();
			logger.info("**** " + "Termina generaci�n de Archivo Consolidado vac�o." + " ****");

			File archivoSalidaDetProceso = new File(
					rutaFinal + File.separator + Constante.PARAMETRO.NOMBRE_ARCHIVO_UNIFICADO_DETPROC
							+ Constante.CARACTERES.GUION_BAJO + fechaArchivo + "." + Constante.EXTENSIONES.TXT);

			logger.info("**** " + "Comprobando Existencia: Ruta Destino de Archivo Detalle del Proceso" + " ****");

			logger.info("**** " + "Generando Archivo Detalle del Proceso vac�o." + " ****");
			FileWriter fwDetProceso = new FileWriter(archivoSalidaDetProceso, true);
			fwDetProceso.write(StringUtils.leftPad("", 185));
			fwDetProceso.write(System.getProperty(Constante.CARACTERES.SEPARADOR_SISTEMA));
			fwDetProceso.close();
			logger.info("**** " + "Termina generaci�n de Archivo Detalle del Proceso vac�o." + " ****");

		} catch (IOException e) {
			logger.error("****" + "{Error en: [Metodo - generateFilesDefault]}" + e.getMessage() + "****");
		}

	}

}
