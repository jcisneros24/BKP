package com.cmc.bkpclient.util;

public abstract class Constante {

	public abstract class PARAMETRO {

		public static final String COD_EJECUCION = "1";
		public static final String ADMIN = "admin";
		public static final String PWD_ADMIN = "admin";
		
		public static final String NOMBRE_PROGRAMA = "FeedBack";
		public static final int TAMA�O_COLUMNA_REPORTE = 20;
		public static final int TAMA�O_LINEA_REPORTE = 479;
		public static final int TAMA�O_NOMBRE_ZIP = 25;
		public static final int TAMA�O_NOMBRE_RAR = 25;
		public static final int TAMA�O_NOMBRE_TXT = 38;
		//para los prefijos EXXXXXX_   .XXX
		public static final int TAMA�O_NOMBRE_PRE_ZIP = 37;
		public static final int TAMA�O_NOMBRE_PRE_RAR = 37;
		public static final int TAMA�O_NOMBRE_PRE_TXT = 50;
		
		
		public static final String DIRECCION_PROPERTIES = "/pr/feedback/online/pe/web/j2ee/config/";
		public static final String ARCHIVO_PROPERTIES = "config.properties";
		public static final String FORMATO_FECHA = "yyMMdd";
		public static final String NOMBRE_ARCHIVO_UNIFICADO = "UNIFICADO_FEEDBACK";
		public static final String NOMBRE_ARCHIVO_UNIFICADO_DETPROC = "DETPROC_FEEDBACK";	

	}

	public abstract class RESPUESTA_SERVICIOS {

		public static final String ESTADO_CORRECTO = "ACTUALIZACION CORRECTA";
		public static final String ESTADO_ERROR = "ERROR";
		public static final String OK = "Se ha actualizado correctamente la informaci�n.";
		public static final String ERROR = "Ha ocurrido un error inesperado.";
		public static final String RUTA_OK = "Se ha actualizado correctamente la ruta de directorios.";
		public static final String RUTA_ERROR_REGEX = "La ruta ingresada no es v�lida.";
		public static final String RUTA_ERROR_VACIA = "La ruta no puede estar vac�a.";
		public static final String FILEDATA_OK = "Se ha actualizado correctamente las propiedades.";
		public static final String ERROR_CAMPO_VACIO_USUARIO = "El campo USUARIO no puede estar vacio.";
		public static final String ERROR_CAMPO_VACIO_GRUPO = "El campo PERFIL no puede estar vacio.";
		public static final String ERROR_CAMPO_VACIO_PASSWORD = "El campo PASSWORD no puede estar vacio.";
		public static final String CREACION_OK = "Se creo correctamente al usuario.";
		public static final String ACTUALIZACION_OK = "Se ha actualizado correctamente el usuario.";
		public static final String USUARIO_EXISTE = "El usuario a crear ya existe.";
		public static final String USUARIO_NO_EXISTE = "Usuario no ha sido definido en configuracion.";
		public static final String USUARIO_NO_AUTENTICADO = "�Usuario no autenticado!";
		public static final String USUARIO_PASSWORD = "Password incorrecto.";
		public static final String USUARIO_VALIDACION_LDAP = "Error en validaci�n LDAP.";

		public static final String ESTADO_CARGA_CORRECTO = "CARGA CORRECTA";
		public static final String ESTADO_CARGA_ERROR = "ERROR CARGA";
		public static final String ARCHIVO_CORRECTO = "Se cargo el archivo correctamente.";
		public static final String ARCHIVO_ERROR = "Hubo un error con el archivo y no se cargo.";
		public static final String ARCHIVO_ERROR_ND = "Ha ocurrido un error inesperado.";
		public static final String ARCHIVO_ERROR_CAMPO_VACIO = " - Campo vac�o en l�nea: ";
		public static final String ARCHIVO_LINEAS_CARGADAS = " - L�neas cargadas: ";
		public static final String ARCHIVO_ERROR_REGEX = " - Error Regex en l�nea: ";
		public static final String ARCHIVO_ERROR_LONGITUD = " - Longitud incorrecta en l�nea: ";
		public static final String ARCHIVO_ERROR_NO_EXISTE_ELEMENTO = " No se seleccion� un archivo a cargar.";
		public static final String ARCHIVO_ERROR_VACIO = " El archivo seleccionado est� vac�o.";
		public static final String CONFIG_OK = "Se actualiz� la configuraci�n correctamente.";
		public static final String CONFIG_EMAIL_ERROR_REGEX = "No es un correo v�lido: ";
		public static final String CONFIG_CRON_ERROR_REGEX_MESES = "No es un valor v�lido para los meses.";
		public static final String CONFIG_CRON_ERROR_REGEX_DIAS = "No es un valor v�lido para los d�as.";

		public static final String JOB_ERROR_ARCHIVO_VACIO = "ERROR: El archivo origen no existe.";
		public static final String JOB_ERROR_ARCHIVO_PESO = "ERROR: Los archivos poseen el mismo peso en bytes: ";
		public static final String JOB_ERROR_DIRECTORIO_NO_EXISTE = "ERROR: Directorio no existe.";
		public static final String PEPS_ERROR_DIRECTORIO_VACIO = "ERROR: El directorio no contiene los archivos esperados.";
		public static final String PEPS_ERROR_COPYDIR = "ERROR: Ha ocurrido un error no esperado al copiar el directorio.";
		public static final String PEPS_ERROR_COPYDIR_IOEXCEPTION = "ERROR: Ha ocurrido un error en el archivo.";
		public static final String JOB_ERROR_ARCHIVO = "ERROR: No existe el archivo.";
		public static final String JOB_ERROR_IOEXCEPTION = "ERROR: Excepcion generada por el manejo de archivos (I/O).";
		public static final String JOB_ERROR_SQLEXCEPTION = "ERROR: Excepcion generada por el acceso a BD (SQL).";
		public static final String JOB_ERROR_ESCRITURA_PERMISO = "ERROR: No se tiene permiso de escritura en directorio!";
		public static final String JOB_ERROR_NO_EXISTE_COINCIDENCIAS = "ERROR: No existe archivo de concidencias anterior.";
		public static final String JOB_ERROR_ARCHIVO_NO_EXISTE = "ERROR: No existe archivos a comparar.";
		public static final String JOB_ERROR_BACKUP_EXISTE = "ERROR: �El backup a generar ya existe para la fecha de hoy!";
		
		public static final String ERROR_PROCESO = "ERROR";
		public static final String JOB_OK_HEAD = "Actualizaci�n Correcta";
		public static final String JOB_OK_BODY = "Se ha actualizado correctamente la programaci�n.";
		public static final String JOB_ERROR_BODY = "Ha ocurrido un error inesperado.";
		public static final String RUTA_CREADA_HEAD = "Proveedor creado.";
		public static final String RUTA_CREADA_BODY = "Se ha registrado correctamente los datos.";
		public static final String RUTA_ACTUALIZADA_HEAD = "Proveedor Actualizado.";
		public static final String RUTA_ACTUALIZADA_BODY = "Se ha actualizado correctamente la ruta de archivo del directorio.";
		public static final String RUTA_ELIMINADA_HEAD = "Proveedor eliminado.";
		public static final String RUTA_ELIMINADA_BODY = "Se ha eliminado correctamente los datos.";
		public static final String RUTA_LONGITUD_CODIGO = "El codigo de proveedor debe cumplir con 12 caracteres.";
		public static final String RUTA_CAMPOS_VACIOS = "Llenar todos los campos.";
		public static final String RUTA_CODIGO_NO_VALIDO= "Por favor ingresar c�digo de proveedor v�lido.";
		public static final String RUTA_CARPETA_NO_VALIDO= "Por favor ingresar ruta de archivo v�lida.";
		public static final String RUTA_CARPETA_EXISTE = "Ruta de archivo existente.";	
		public static final String RUTA_CODIGO_EXISTE = "C�digo de proveedor existente.";
		
	}
	
	public abstract class EXTENSIONES {

		public static final String ZIP = "zip";
		public static final String RAR = "rar";
		public static final String TXT = "txt";
	}

	public abstract class DESARROLLO {

		public static final String CORREO = "cda.swf.distribuido@gmail.com";
		public static final String CORREO_CLAVE = "cda12345!";
		public static final String ESPACIO = " ";

	}

	public abstract class CONFIGURACION {
		
		public static final int LONGITUD_NOMBRE_ARCHIVO_PROVEEDOR = 80;

		public static final String PREFIJO_RUTA_ENTRADA = 
				"PROV_";
		
		public static final String RUTA_DIRECTORIO_SALIDA_XCOM = 
				"RUTA_DIRECTORIO_SALIDA_XCOM";
		
		public static final String RUTA_DIRECTORIO_AUXILIAR = 
				"RUTA_DIRECTORIO_AUXILIAR";

		public static final String EXPRESION_CRON_SEGUNDO = 
				"EXPRESION_CRON_SEGUNDO";
		
		public static final String EXPRESION_CRON_HORA = 
				"EXPRESION_CRON_HORA";
		
		public static final String EXPRESION_CRON_MINUTO = 
				"EXPRESION_CRON_MINUTO";
		
		public static final String EXPRESION_CRON_DIA_SEMANA = 
				"EXPRESION_CRON_DIA_SEMANA";
		
		public static final String EXPRESION_CRON_DIA = 
				"EXPRESION_CRON_DIA";
		
		public static final String EXPRESION_CRON_MES = 
				"EXPRESION_CRON_MES";
		
		public static final String EXPRESION_CRON_ESTADO = 
				"EXPRESION_CRON_ESTADO";
		
		/******** VALORES PARA CONFIGURAR PROPERTIES INICIAL **********/	
		
		public static final String VALUE_RUTA_DIRECTORIO_SALIDA_XCOM = 
				"/pr/feedback/online/pe/web/j2ee/XCOM/"; //"/mnt/compartido/feedback/XCOM/";
		
		public static final String VALUE_RUTA_DIRECTORIO_AUXILIAR = 
				"/pr/feedback/online/pe/web/j2ee/TempFiles/";

		public static final String VALUE_EXPRESION_CRON_SEGUNDO = 
				"0";
		
		public static final String VALUE_EXPRESION_CRON_HORA = 
				"*"; //"21";
		
		public static final String VALUE_EXPRESION_CRON_MINUTO = 
				"*";//"0";
		
		public static final String VALUE_EXPRESION_CRON_DIA_SEMANA = 
				"?";
		
		public static final String VALUE_EXPRESION_CRON_DIA = 
				"*";
		
		public static final String VALUE_EXPRESION_CRON_MES = 
				"*";
		
		public static final String VALUE_EXPRESION_CRON_ESTADO = 
				"A";

	}
	
	public abstract class ERROR_FUNCIONAL {

		public static final String EF_CREACION_DIRECTORIO = "No se pudo crear Directorio de Archivo Properties.";
		public static final String EF_CREACION_ARCHIVO_PROPERTIES = "No se pudo crear Archivo Properties.";
		public static final String EF_NO_EXISTE_DIRECTORIO = "El directorio de entrada no existe en properties.";
		public static final String EF_NOMBRE_ESTANDAR_EXTENSION = "Extension no valida en el archivo: ";
		public static final String EF_NOMBRE_ESTANDAR_LONGITUD = "Longitud incorrecta de caracteres en el archivo: ";
		public static final String EF_NOMBRE_ESTANDAR_FECHA = "Fecha no valida en el nombre del archivo: ";
		public static final String EF_NOMBRE_ESTANDAR_ESPACIOS = "Espacios o valores no validos en el nombre del archivo: ";
		public static final String EF_LISTA_ARCHIVOS_VACIA = "No existen archivos en los directorios.";
		public static final String EF_MENSAJE_VALIDACION = "El nombre del Archivo no cumple el estandar.";
		
	}

	public abstract class ESTADO {

		public static final String ACTIVO = "A";
		public static final String INACTIVO = "I";

	}
	
	public abstract class CARACTERES {
		
		public static final String PIPELINE = "\\|";
		public static final String SEPARADOR_SISTEMA = "line.separator";
		public static final String ESPACIO_GRANDE = "   ";
		public static final String GUION_BAJO = "_";
		public static final String VACIO = " ";
		
	}

	public abstract class REGEX {
		public static final String ALFANUMERICO = "(?i)^[a-z\u00E0-\u00FC_\\-\\\\/ :\\d+]+$";
		public static final String WINDOWS_PATH = "([A-Z|a-z]:\\\\[^*|\"<>?\\n]*)|(\\\\\\\\.*?\\\\.*)";
		public static final String LINUX_PATH = "^(/[^/ ]*)+/?$";
		public static final String FILE_NAME = ".*([ \t]).*";
		public static final String EMAIL = ".+@.+";
		public static final String DIAS = "^\\d+$";
		public static final String HORAS = "^([1-9]|1[0-9]|2[0-3])$";
		
	}

}
