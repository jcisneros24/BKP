package com.cmc.bkpclient.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ValidacionesUtil {

	private static final Logger logger = LogManager.getLogger(ValidacionesUtil.class);

	public static boolean crearProperties(File pFile) throws FileNotFoundException, IOException {

		Properties propiedad = new Properties();
		OutputStream output = null;
		boolean propertiesCreado = false;

		try {

			// Validamos existencia de archivo properties en directorio
			if (!pFile.exists()) {

				// Crear Archivo Properties
				output = new FileOutputStream(pFile);

				// Establecer propiedades por defecto

				propiedad.setProperty("RUTA_DIRECTORIO_AUXILIAR",
						Constante.CONFIGURACION.VALUE_RUTA_DIRECTORIO_AUXILIAR);
				propiedad.setProperty("RUTA_DIRECTORIO_SALIDA_XCOM",
						Constante.CONFIGURACION.VALUE_RUTA_DIRECTORIO_SALIDA_XCOM);
				propiedad.setProperty("EXPRESION_CRON_SEGUNDO", Constante.CONFIGURACION.VALUE_EXPRESION_CRON_SEGUNDO);
				propiedad.setProperty("EXPRESION_CRON_MINUTO", Constante.CONFIGURACION.VALUE_EXPRESION_CRON_MINUTO);
				propiedad.setProperty("EXPRESION_CRON_HORA", Constante.CONFIGURACION.VALUE_EXPRESION_CRON_HORA);
				propiedad.setProperty("EXPRESION_CRON_DIA_SEMANA",
						Constante.CONFIGURACION.VALUE_EXPRESION_CRON_DIA_SEMANA);
				propiedad.setProperty("EXPRESION_CRON_DIA", Constante.CONFIGURACION.VALUE_EXPRESION_CRON_DIA);
				propiedad.setProperty("EXPRESION_CRON_MES", Constante.CONFIGURACION.VALUE_EXPRESION_CRON_MES);
				propiedad.setProperty("EXPRESION_CRON_ESTADO", Constante.CONFIGURACION.VALUE_EXPRESION_CRON_ESTADO);

				// Grabar propiedades del Archivo Properties
				propiedad.store(output, null);
				output.close();

				logger.info("****" + " Archivo Properties Creado " + "**** ");
				propertiesCreado = true;

			} else {

				logger.info("****" + " Atencion!...Ya existe un Archivo Properties " + "**** ");
				propertiesCreado = false;
			}

		} catch (FileNotFoundException ex) {
			logger.error("****" + "{Error en: [Metodo - loadPropertiesFile]}" + ex.getMessage() + "****");
		} catch (IOException e) {
			logger.error("****" + "{Error en: [Metodo - loadPropertiesFile]}" + e.getMessage() + "****");
		}
		return propertiesCreado;
	}

	public static boolean crearDirectorio(File directorio) {

		boolean directorioCreado = false;

		// Validamos existencia de directorio en clase
		if (!directorio.exists()) {

			// Validamos existencia de directorio en fisico
			if (directorio.mkdir()) {
				logger.info("****" + " Directorio creado. " + "**** " + directorio);
				directorioCreado = true;
			}
		} else {

			logger.info("****" + " Atencion!...Ya existe el directorio a crear. " + "**** ");

			directorioCreado = false;
		}

		return directorioCreado;
	}

	public static boolean validarPropiedadObligatoria(String key) {

		logger.info("****" + " Ejecucion de metodo: [validarPropiedadObligatoria] " + "****");
		logger.info("****" + " Inicia Validacion de Propiedades Obligatorias" + "****");

		boolean rpta = true;

		if (key.equals((Constante.CONFIGURACION.EXPRESION_CRON_DIA))
				|| key.equals(Constante.CONFIGURACION.EXPRESION_CRON_DIA_SEMANA)
				|| key.equals(Constante.CONFIGURACION.EXPRESION_CRON_HORA)
				|| key.equals(Constante.CONFIGURACION.EXPRESION_CRON_MES)
				|| key.equals(Constante.CONFIGURACION.EXPRESION_CRON_MINUTO)
				|| key.equals(Constante.CONFIGURACION.EXPRESION_CRON_SEGUNDO)
				|| key.equals(Constante.CONFIGURACION.EXPRESION_CRON_ESTADO)
				|| key.equals(Constante.CONFIGURACION.RUTA_DIRECTORIO_AUXILIAR)
				|| key.equals(Constante.CONFIGURACION.RUTA_DIRECTORIO_SALIDA_XCOM)) {

			return rpta;
		}

		return false;

	}

	public static String validateNullOrEmpty(String field) {

		field = field.trim();

		return (field == null || field.isEmpty()) ? Constante.CARACTERES.VACIO : field;

	}

	public static String validateLong(String field, int longitud) {

		if (field.length() > longitud) {

			logger.info("****" + " El campo no cuenta con los caracteres validos: " + field + "**** ");

		}
		return field;
	}

	public static boolean validateFileNameStandard(File fileEntry) {

		boolean nombreArchivoValidado = false;
		String fechaHoy = DateUtils.getDateFormat();

		String extension = FilenameUtils.getExtension(fileEntry.getName());
		logger.info("****" + " EXTENSION: " + extension + "****");

		// valida que el nombre del archivo tenga la extension correcta
		if (extension.equalsIgnoreCase(Constante.EXTENSIONES.ZIP)) {
			logger.info(" wuc =>if (");

			// valida que el nombre del archivo tenga el tama�o de caracteres
			// especifico
			if ((fileEntry.getName().length() == Constante.PARAMETRO.TAMA�O_NOMBRE_ZIP)
					|| (fileEntry.getName().length() == Constante.PARAMETRO.TAMA�O_NOMBRE_PRE_ZIP)) {

				// valida que el nombre archivo tenga la fecha actual
				if (fileEntry.getName().contains(fechaHoy)) {

					// valida que el nombre archivo tenga el formato estandar
					if (!fileEntry.getName().matches(Constante.REGEX.FILE_NAME)) {

						nombreArchivoValidado = true;

					} else {
						logger.info("**** " + Constante.ERROR_FUNCIONAL.EF_NOMBRE_ESTANDAR_ESPACIOS + " ****");
					}

				} else {
					logger.info("**** " + Constante.ERROR_FUNCIONAL.EF_NOMBRE_ESTANDAR_FECHA + fileEntry.getName()
							+ " ****");
				}

			} else {

				logger.info("**** " + Constante.ERROR_FUNCIONAL.EF_NOMBRE_ESTANDAR_LONGITUD + " ****");
			}

		} else if (extension.equalsIgnoreCase(Constante.EXTENSIONES.RAR)) {

			// valida que el nombre del archivo tenga el tama�o de caracteres
			// especifico
			if ((fileEntry.getName().length() == Constante.PARAMETRO.TAMA�O_NOMBRE_RAR)
					|| (fileEntry.getName().length() == Constante.PARAMETRO.TAMA�O_NOMBRE_PRE_RAR)) {

				// valida que el nombre archivo tenga la fecha actual
				if (fileEntry.getName().contains(fechaHoy)) {

					// valida que el nombre archivo tenga el formato estandar
					if (!fileEntry.getName().matches(Constante.REGEX.FILE_NAME)) {

						nombreArchivoValidado = true;

					} else {
						logger.info("**** " + Constante.ERROR_FUNCIONAL.EF_NOMBRE_ESTANDAR_ESPACIOS + " ****");
					}

				} else {
					logger.info("**** " + Constante.ERROR_FUNCIONAL.EF_NOMBRE_ESTANDAR_FECHA + fileEntry.getName()
							+ " ****");
				}
			} else {

				logger.info("**** " + Constante.ERROR_FUNCIONAL.EF_NOMBRE_ESTANDAR_LONGITUD + " ****");
			}

		} else if (extension.equalsIgnoreCase(Constante.EXTENSIONES.TXT)) {
			// wuc - TEXT
			String sCadenaT = "";
			String sSubCadenaT = "";
			// wuc - sacamos la parte de prefijo
			if (fileEntry.getName().length() == Constante.PARAMETRO.TAMA�O_NOMBRE_PRE_TXT) {

				sCadenaT = fileEntry.getName();
				sSubCadenaT = sCadenaT.substring(8, 46);
				logger.info("****sSubCadenaT " + sSubCadenaT + " ****");

				// valida que el nombre archivo tenga la fecha actual
				if (sSubCadenaT.contains(fechaHoy)) {

					// valida que el nombre archivo tenga el formato estandar
					// wuc - comente y coloque otra validacion
					// if
					// (!fileEntry.getName().matches(Constante.REGEX.FILE_NAME))
					// {
					if (!sSubCadenaT.matches(Constante.REGEX.FILE_NAME)) {

						nombreArchivoValidado = true;

					} else {
						logger.info("**** " + Constante.ERROR_FUNCIONAL.EF_NOMBRE_ESTANDAR_ESPACIOS + " ****");
					}

				} else {

					logger.info("**** " + Constante.ERROR_FUNCIONAL.EF_NOMBRE_ESTANDAR_FECHA + sSubCadenaT + " ****");
				}

			} else {

				logger.info("wuc => else  no modificado fileEntry.getName().length() == TAMA�O_NOMBRE_TXT");
				// valida que el nombre del archivo tenga el tama�o de
				// caracteres especifico
				if (fileEntry.getName().length() == Constante.PARAMETRO.TAMA�O_NOMBRE_TXT) {

					// valida que el nombre archivo tenga la fecha actual
					if (fileEntry.getName().contains(fechaHoy)) {

						// valida que el nombre archivo tenga el formato
						// estandar
						if (!fileEntry.getName().matches(Constante.REGEX.FILE_NAME)) {

							nombreArchivoValidado = true;

						} else {
							logger.info("**** " + Constante.ERROR_FUNCIONAL.EF_NOMBRE_ESTANDAR_ESPACIOS + " ****");
						}

					} else {
						logger.info("**** " + Constante.ERROR_FUNCIONAL.EF_NOMBRE_ESTANDAR_FECHA + fileEntry.getName()
								+ " ****");
					}

				} else {

					logger.info("**** " + Constante.ERROR_FUNCIONAL.EF_NOMBRE_ESTANDAR_LONGITUD + " ****");
				}
			}

		} else {

			logger.info(
					"**** " + Constante.ERROR_FUNCIONAL.EF_NOMBRE_ESTANDAR_EXTENSION + fileEntry.getName() + " ****");
		}

		return nombreArchivoValidado;
	}

}
