package com.cmc.bkpclient.util;

public class RespuestaUtil {
	
	private String respuesta;
	private String data;
	
	public String getRespuesta() {
		return respuesta;
	}
	
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	
	public String getData() {
		return data;
	}
	
	public void setData(String data) {
		this.data = data;
	}
	
	public RespuestaUtil(String respuesta, String data) {
		super();
		this.respuesta = respuesta;
		this.data = data;
	}

	public RespuestaUtil() {
	}
	
	
}
