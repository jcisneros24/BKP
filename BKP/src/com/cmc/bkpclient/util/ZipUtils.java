package com.cmc.bkpclient.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.cmc.bkpclient.dao.PropertiesDaoImpl;
import com.cmc.bkpclient.model.FolderDTO;

public class ZipUtils {
	
	private static final Logger logger = LogManager.getLogger(ZipUtils.class);

	PropertiesDaoImpl propertiesDaoImpl = new PropertiesDaoImpl();
	PropertiesUtil propertiesUtil = new PropertiesUtil();
	FolderDTO folderDTO = new FolderDTO();
	
	String rutaAuxiliarZIP = null;
		
	public ZipUtils() throws IOException {	
		
		folderDTO = propertiesDaoImpl.getFolderPath();	
		rutaAuxiliarZIP = folderDTO.getTemporal();
	}
	
	public void unZipIt(String zipFile) {
		
		logger.info("**** " + "Inicia Extraccion de Archivos ZIP" + " ****");

		byte[] buffer = new byte[1024];

		try {
		
			ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));
			ZipEntry ze = zis.getNextEntry();

			while (ze != null) {

				String fileName = ze.getName();
				File fileComprimido = new File(rutaAuxiliarZIP + File.separator + fileName);
				new File(fileComprimido.getParent()).mkdirs();
				FileOutputStream fos_directorio = new FileOutputStream(fileComprimido);
				int len;
				while ((len = zis.read(buffer)) > 0) {
					fos_directorio.write(buffer,0,len);
				}
				fos_directorio.close();
				ze = zis.getNextEntry();

			}
			zis.closeEntry();
			zis.close();

		} catch (IOException ex) {
			logger.error("****" + "{Error en: [Metodo - unZipIt]}" + ex.getMessage() + "****");
		}
	}
	
	public List<File> leerArchivosZIP(){		
		
		logger.info("**** " + "Inicia Lectura de  Archivos ZIP" + " ****");
		
		List<File> leerArchivosZIP = new ArrayList<File>();		
		File directorioAuxiliar = new File(rutaAuxiliarZIP);
		File[] archivosDescomprimidos = directorioAuxiliar.listFiles();
		
		for (File file : archivosDescomprimidos) {
			
				leerArchivosZIP.add(file);					
		}
		
		return leerArchivosZIP;			
	}

}