package com.cmc.bkpclient.dao;

import java.io.FileNotFoundException;
import java.io.IOException;

import com.cmc.bkpclient.bean.BaseBean;
import com.cmc.bkpclient.model.CronDTO;
import com.cmc.bkpclient.model.FolderDTO;

public interface PropertiesDao {

	void createPropiedad(String key, String value) throws FileNotFoundException, IOException;

	BaseBean readPropiedad(String key) throws IOException;

	void updatePropiedad(String key, String value) throws FileNotFoundException, IOException;

	void deletePropiedad(String key) throws FileNotFoundException, IOException;

	void updateCRON(CronDTO cron) throws IOException;

	CronDTO getCRON();
	
	FolderDTO getFolderPath();

}
