package com.cmc.bkpclient.dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.cmc.bkpclient.bean.BaseBean;
import com.cmc.bkpclient.model.CronDTO;
import com.cmc.bkpclient.model.FolderDTO;
import com.cmc.bkpclient.util.Constante;
import com.cmc.bkpclient.util.PropertiesUtil;

public class PropertiesDaoImpl implements PropertiesDao {

	private static final Logger logger = LogManager.getLogger(PropertiesDaoImpl.class);
				
	PropertiesUtil propertiesUtil = new PropertiesUtil();
	Properties propiedad = null;
	final File file = new File(Constante.PARAMETRO.DIRECCION_PROPERTIES + 
			Constante.PARAMETRO.ARCHIVO_PROPERTIES);

	@Override
	public void createPropiedad(String key, String value) throws FileNotFoundException, IOException {

		logger.info("****" + " Agrega Propiedades en Archivo Properties " + "****");
		FileOutputStream output = null;
		
		try {
			propiedad = propertiesUtil. loadPropertiesFile();
			logger.info("****" + " Validando existencia de directorio properties... " + "**** ");
			
			if(file.exists()){
				logger.info("****" + " Inicia Carga de propiedades de Archivo Properties " + "****");
				output = new FileOutputStream(file, false);
				propiedad.setProperty(key, value);
				propiedad.store(output, null);
				output.close();
				
			} else {
				logger.info("****" + " No se creo la propiedad por no existir directorio properties. " + "****");
			}
			
		} catch (FileNotFoundException ex) {
			logger.error("****" + "{Error en: [Metodo - createPropiedad]}" + ex.getMessage() + "****");
		} catch (IOException e) {
			logger.error("****" + "{Error en: [Metodo - createPropiedad]}" + e.getMessage() + "****");
		}
	}
	
	@Override
	public BaseBean readPropiedad(String key) throws IOException {
		
		BaseBean baseBean = new BaseBean();
		logger.info("****" + " Obtiene propiedades por Key en Archivo Properties " + "****");
		try {
			
			propiedad = propertiesUtil. loadPropertiesFile();
			logger.info("****" + " Validando existencia de directorio properties... " + "**** ");

			if(file.exists()){
				logger.info("****" + " Inicia lectura de propiedades de Archivo Properties " + "****");
				String valor = (String) propiedad.get(key);
				baseBean.setKey(key);
				baseBean.setValue(valor);
			} else {
				logger.info("****" + " No se obtuvo la propiedad por no existir directorio properties. " + "****");
			}
			
		} catch (FileNotFoundException ex) {
			logger.error("****" + "{Error en: [Metodo - getPropiedad]}" + ex.getMessage() + "****");
		} catch (IOException e) {
			logger.error("****" + "{Error en: [Metodo - getPropiedad]}" + e.getMessage() + "****");
		}
		
		return baseBean;
	}
	
	@Override
	public void updatePropiedad(String key, String value) throws FileNotFoundException, IOException {

		logger.info("****" + " Actualiza propiedades en Archivo Properties " + "****");
		FileOutputStream output = null;
		try {

			propiedad = propertiesUtil. loadPropertiesFile();
			logger.info("****" + " Validando existencia de directorio properties... " + "**** ");
			
			if(file.exists()){
				logger.info("****" + " Inicia actualizacion de propiedades de Archivo Properties " + "****");
				output = new FileOutputStream(file);
				propiedad.setProperty(key, value);
				propiedad.store(output, null);
				output.close();
			} else {
				logger.info("****" + " No se actualizo la propiedad por no existir directorio properties. " + "****");
			}
						
		} catch (FileNotFoundException ex) {
			logger.error("****" + "{Error en: [Metodo - updatePropiedad]}" + ex.getMessage() + "****");
		} catch (IOException e) {
			logger.error("****" + "{Error en: [Metodo - updatePropiedad]}" + e.getMessage() + "****");
		}

	}
	
	@Override
	public void deletePropiedad(String key) throws FileNotFoundException, IOException {

		logger.info("****" + " Elimina propiedades en Archivo Properties " + "****");
		FileOutputStream output = null;
		try {

			propiedad = propertiesUtil. loadPropertiesFile();
			logger.info("****" + " Validando existencia de directorio properties... " + "**** ");
			
			if(file.exists()){
				logger.info("****" + " Inicia eliminacion de propiedades de Archivo Properties " + "****");
				propiedad.remove(key);
				output = new FileOutputStream(file, false);
				propiedad.store(output, null);
				output.close();
			} else {
				logger.info("****" + " No se elimino la propiedad por no existir directorio properties. " + "****");
			}

		} catch (FileNotFoundException ex) {
			logger.error("****" + "{Error en: [Metodo - deletePropiedad]}" + ex.getMessage() + "****");
		} catch (IOException e) {
			logger.error("****" + "{Error en: [Metodo - deletePropiedad]}" + e.getMessage() + "****");
		}
	}
	
	@Override
	public void updateCRON(CronDTO cron) throws IOException{
				
		try {
			createPropiedad(  
					Constante.CONFIGURACION.EXPRESION_CRON_SEGUNDO, cron.getSegundos());
			createPropiedad( 
					Constante.CONFIGURACION.EXPRESION_CRON_MINUTO, cron.getMinutos());
			createPropiedad( 
					Constante.CONFIGURACION.EXPRESION_CRON_DIA_SEMANA, cron.getDia_semana());
			createPropiedad( 
					Constante.CONFIGURACION.EXPRESION_CRON_DIA, cron.getDias());
			createPropiedad( 
					Constante.CONFIGURACION.EXPRESION_CRON_HORA, cron.getHoras());
			createPropiedad(  
					Constante.CONFIGURACION.EXPRESION_CRON_MES, cron.getMeses());
			createPropiedad(  
					Constante.CONFIGURACION.EXPRESION_CRON_ESTADO, cron.getEstado());
		} catch (IOException e) {
			logger.error("****" + "{Error en: [Metodo - updateCRON]}" + e.getMessage() + "****");
		}
		
	}

	@Override
	public CronDTO getCRON() {
		
		CronDTO cronDTO = new CronDTO();
		
		try {
			
			String segundos = readPropiedad( 
					Constante.CONFIGURACION.EXPRESION_CRON_SEGUNDO).toString();
			String minutos = readPropiedad( 
					Constante.CONFIGURACION.EXPRESION_CRON_MINUTO).toString();
			String dia_semana = readPropiedad( 
					Constante.CONFIGURACION.EXPRESION_CRON_DIA_SEMANA).toString();
			String dias = readPropiedad( 
					Constante.CONFIGURACION.EXPRESION_CRON_DIA).toString();
			String horas = readPropiedad( 
					Constante.CONFIGURACION.EXPRESION_CRON_HORA).toString();
			String meses = readPropiedad(  
					Constante.CONFIGURACION.EXPRESION_CRON_MES).toString();
			String estado = readPropiedad(  
					Constante.CONFIGURACION.EXPRESION_CRON_ESTADO).toString();
					
			cronDTO.setSegundos(segundos);
			cronDTO.setMinutos(minutos);
			cronDTO.setDia_semana(dia_semana);
			cronDTO.setDias(dias);
			cronDTO.setHoras(horas);
			cronDTO.setMeses(meses);
			cronDTO.setEstado(estado);
			cronDTO.setDescripcion(Constante.PARAMETRO.NOMBRE_PROGRAMA);
			
		} catch (IOException e) {
			logger.error("****" + "{Error en: [Metodo - getCRON]}" + e.getMessage() + "****");
		}
		
		return cronDTO;
	}
	
	@Override
	public FolderDTO getFolderPath() {
		
		FolderDTO folderDTO = new FolderDTO();
					
		try {
			String rutaTemporal = readPropiedad(
					Constante.CONFIGURACION.RUTA_DIRECTORIO_AUXILIAR).toString();			
			String rutaXCOM = readPropiedad(
					Constante.CONFIGURACION.RUTA_DIRECTORIO_SALIDA_XCOM).toString();
		    		    
			folderDTO.setTemporal(rutaTemporal);
			folderDTO.setDestino(rutaXCOM);
		    
		} catch (IOException e) {
			logger.error("****" + "{Error en: [Metodo - getFolderPath]}" + e.getMessage() + "****");
		}
		
		return folderDTO;
	}

}
