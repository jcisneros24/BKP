package com.cmc.bkpclient.job.impl;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cmc.bkpclient.dao.PropertiesDaoImpl;
import com.cmc.bkpclient.model.FolderDTO;
import com.cmc.bkpclient.util.Constante;
import com.cmc.bkpclient.util.DateUtils;
import com.cmc.bkpclient.util.PropertiesUtil;
import com.cmc.bkpclient.util.RarUtils;
import com.cmc.bkpclient.util.TxtUtils;
import com.cmc.bkpclient.util.ValidacionesUtil;
import com.cmc.bkpclient.util.ZipUtils;

public class GenerateConsolidatedJobImpl {

	private static Logger logger = LoggerFactory.getLogger(GenerateConsolidatedJobImpl.class);

	PropertiesDaoImpl propertiesDaoImpl = new PropertiesDaoImpl();
	PropertiesUtil propertiesUtil = new PropertiesUtil();
	FolderDTO folderDTO = new FolderDTO();
	List<Character> rutasDirectorios = new ArrayList<Character>();

	String tempFolder = null;
	String rutaXCOM = null;
	Properties props = null;
	File folder = null;

	public GenerateConsolidatedJobImpl() throws IOException {

		/** Configuracion de Entorno **/
		logger.info("****" + " Carga Configuracion de Entorno: Archivo Properties y Carpetas Obligatorias " + "****");

		logger.info("**** " + "Inicia carga de Archivo Properties." + " ****");
		props = propertiesUtil.loadPropertiesFile();
		logger.info("**** " + "Termina carga de Archivo Properties." + " ****");

		logger.info("**** " + "Inicia creacion de Carpetas obligatorias." + " ****");
		propertiesUtil.loadFolderRequired();
		logger.info("**** " + "Termina creacion de Carpetas obligatorias." + " ****");

		folderDTO = propertiesDaoImpl.getFolderPath();
		tempFolder = folderDTO.getTemporal();// Ruta estatica de carpeta temporal
		rutaXCOM = folderDTO.getDestino();// Ruta estatica de carpeta salida
	}

	public void execute() {

		try {

			logger.info("**** " + "Validando existencia de directorio de entrada en Archivo Properties..." + " ****");

			for (String key : props.stringPropertyNames()) {

				String value = props.getProperty(key);
				folder = new File(value);

				if (key.contains(Constante.CONFIGURACION.PREFIJO_RUTA_ENTRADA)) {

					if (folder.exists()) {

						listFilesForFolder(folder);

					} else {

						logger.info("**** " + Constante.ERROR_FUNCIONAL.EF_NO_EXISTE_DIRECTORIO + " ****");
					}
				}
			}

		} catch (IOException e) {
			logger.error("****" + " Error inesperado: " + e.getMessage() + "****");
		} catch (ParseException e) {
			logger.error("****" + " Error inesperado: " + e.getMessage() + "****");
		}
	}

	private void listFilesForFolder(File folder) throws IOException, ParseException {

		logger.info("**** " + "Lista de Archivos a Consolidar." + " ****");

		ZipUtils zipUtils = new ZipUtils();
		RarUtils rarUtils = new RarUtils();
		String fechaHoy = DateUtils.getDateFormat();

		List<File> archivosTexto = new ArrayList<File>();
		logger.info("**** " + "La cantidad de Archivos en el Directorio: " + folder.getName() + "es: "
				+ folder.listFiles().length + " ****");

		for (final File fileEntry : folder.listFiles()) {

			logger.info("****" + " Archivo FileEntry en ListFiles: " + fileEntry.getName() + "****");

			if (ValidacionesUtil.validateFileNameStandard(fileEntry)) {

				String extension = FilenameUtils.getExtension(fileEntry.getName());
				logger.info("****" + " EXTENSION: " + extension + "****");

				if (extension.equalsIgnoreCase(Constante.EXTENSIONES.ZIP)) {

					// Extrae archivos txt de un Archivo ZIP
					zipUtils.unZipIt(fileEntry.getPath());

				} else if (extension.equalsIgnoreCase(Constante.EXTENSIONES.RAR)) {

					// Extrae archivos txt de un Archivo RAR
					rarUtils.extractFiles(fileEntry);

				} else if (extension.equalsIgnoreCase(Constante.EXTENSIONES.TXT)) {

					archivosTexto.add(fileEntry);

				} else {
					logger.info("**** " + Constante.ERROR_FUNCIONAL.EF_NOMBRE_ESTANDAR_EXTENSION + fileEntry.getName()
							+ " ****");
				}

			} else {
				logger.info("**** " + Constante.ERROR_FUNCIONAL.EF_MENSAJE_VALIDACION + fileEntry.getName() + " ****");
			}
		}

		// Carga de txt en una lista: lstFile
		File dirTemp = new File(Constante.CONFIGURACION.VALUE_RUTA_DIRECTORIO_AUXILIAR);
		File[] lstTXT = dirTemp.listFiles();
		List<File> lstFile = new ArrayList<File>();

		for (int i = 0; i < lstTXT.length; i++) {

			if (lstTXT[i].getName().contains(fechaHoy)) {

				if (ValidacionesUtil.validateFileNameStandard(lstTXT[i])) {

					lstFile.add(lstTXT[i]);

				} else {
					logger.info(
							"**** " + Constante.ERROR_FUNCIONAL.EF_MENSAJE_VALIDACION + lstTXT[i].getName() + " ****");
				}

			}
		}

		for (int i = 0; i < archivosTexto.size(); i++) {

			if (ValidacionesUtil.validateFileNameStandard(archivosTexto.get(i))) {

				lstFile.add(archivosTexto.get(i));

			} else {
				logger.info("**** " + Constante.ERROR_FUNCIONAL.EF_MENSAJE_VALIDACION + archivosTexto.get(i).getName()
						+ " ****");
			}

		}

		// Generacion de txt consolidado y detalle del proceso
		TxtUtils.filesGeneration(lstFile, rutaXCOM, folder);

		// Limpieza de carpeta temporal
		File archivosTemp = new File(tempFolder);
		FileUtils.cleanDirectory(archivosTemp);

	}

}
