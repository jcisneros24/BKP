package com.cmc.bkpclient.job.scheduler;

import static org.quartz.TriggerBuilder.newTrigger;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.StdSchedulerFactory;

import com.cmc.bkpclient.model.CronDTO;
import com.cmc.bkpclient.dao.PropertiesDaoImpl;
import com.cmc.bkpclient.job.GenerateConsolidatedJob;
import com.cmc.bkpclient.util.Constante;

public class SchedulerMain {
	
	private static final Logger logger = LogManager.getLogger(SchedulerMain.class);
	
	public static void runScheduler() throws Exception {
		logger.info("**** " + "Inicia carga de Expresiones CRON." + " ****");
		try {
			
			PropertiesDaoImpl propertiesDaoImpl = new PropertiesDaoImpl();
			
			CronDTO cronDTO = new CronDTO();
			
			String segundos = propertiesDaoImpl.readPropiedad( 
					Constante.CONFIGURACION.EXPRESION_CRON_SEGUNDO).toString();
			String minutos = propertiesDaoImpl.readPropiedad( 
					Constante.CONFIGURACION.EXPRESION_CRON_MINUTO).toString();
			String dia_semana = propertiesDaoImpl.readPropiedad( 
					Constante.CONFIGURACION.EXPRESION_CRON_DIA_SEMANA).toString();
			String dias = propertiesDaoImpl.readPropiedad( 
					Constante.CONFIGURACION.EXPRESION_CRON_DIA).toString();
			String horas = propertiesDaoImpl.readPropiedad( 
					Constante.CONFIGURACION.EXPRESION_CRON_HORA).toString();
			String meses = propertiesDaoImpl.readPropiedad(  
					Constante.CONFIGURACION.EXPRESION_CRON_MES).toString();
			String estado = propertiesDaoImpl.readPropiedad(  
					Constante.CONFIGURACION.EXPRESION_CRON_ESTADO).toString();
					
			cronDTO.setSegundos(segundos);
			cronDTO.setDia_semana(dia_semana);
			cronDTO.setHoras(horas);
			cronDTO.setMinutos(minutos);
			cronDTO.setDias(dias);
			cronDTO.setMeses(meses);
			cronDTO.setEstado(estado);
			cronDTO.setDescripcion(Constante.PARAMETRO.NOMBRE_PROGRAMA);
			
			logger.info("****" + " EXPRESION CRON: "  + cronDTO.toString() + "****");
			logger.info("**** " + "Termina carga de Expresiones CRON." + " ****");
			
			JobDetail jobDetail = initializeJobDetail(cronDTO);
			Trigger trigger = initializeTrigger(cronDTO);
			executeScheduler(jobDetail, trigger);
		} catch (IOException e) {
			logger.error("****" + "{Error en: [Metodo - runScheduler]}" + e.getMessage() + "****");
		}

	}

	public static void executeScheduler(JobDetail job, Trigger trigger)
			throws ClassNotFoundException, SchedulerException {

		Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
		scheduler.scheduleJob(job, trigger);
		scheduler.start();
		
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void updateScheduler(CronDTO cron) throws SchedulerException {

		Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
		Trigger oldTrigger = scheduler.getTrigger(TriggerKey.triggerKey(cron.getDescripcion()));
		TriggerBuilder tb = oldTrigger.getTriggerBuilder();
		Trigger newtrigger = tb.withSchedule(CronScheduleBuilder.cronSchedule(cron.toString())).build();
		scheduler.rescheduleJob(oldTrigger.getKey(), newtrigger);
	}

	public static void deleteScheduler(CronDTO cron) throws SchedulerException {

		Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
		scheduler.unscheduleJob(TriggerKey.triggerKey(cron.getDescripcion()));

	}

	public static JobDetail initializeJobDetail(CronDTO cron) {
		
		logger.info("****" + " Se inicializa Job: GenerateConsolidatedJob " + "****");
		
		JobKey jobkey = new JobKey(cron.getDescripcion());
		JobDetail jobDetail = null;
		
		if(cron.getDescripcion().equals(Constante.PARAMETRO.NOMBRE_PROGRAMA)){
			
			jobDetail = JobBuilder.newJob(GenerateConsolidatedJob.class).
					withIdentity(jobkey).build();
		}
		
		return jobDetail;

	}

	public static Trigger initializeTrigger(CronDTO cronDTO) {
		
		TriggerKey triggerKey = new TriggerKey(cronDTO.getDescripcion());
		return newTrigger().startNow().withIdentity(triggerKey)
				.withSchedule(CronScheduleBuilder.cronSchedule(cronDTO.toString())).build();
	}

}
