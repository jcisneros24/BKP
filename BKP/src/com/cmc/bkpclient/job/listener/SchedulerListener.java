package com.cmc.bkpclient.job.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.cmc.bkpclient.job.scheduler.SchedulerMain;

/**
 * Application Lifecycle Listener implementation class SchedulerListener
 *
 */
@WebListener
public class SchedulerListener implements ServletContextListener {

	private static final Logger logger = LogManager.getLogger(SchedulerListener.class);

	/**
	 * Default constructor.
	 */
	public SchedulerListener() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		logger.info("****" + " Inicia Proceso Distribuido FeedBack " + "****");

		try {

			/** Configuracion de CRON **/
			logger.info("****" + " Carga Configuracion de CRON: Expresiones CRON " + "****");

			SchedulerMain.runScheduler();

		} catch (Exception e) {
			logger.info("****" + " Error en metodo: [contextInitialized] " + "****" + e.getMessage());
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		// TODO Auto-generated method stub

	}

}
