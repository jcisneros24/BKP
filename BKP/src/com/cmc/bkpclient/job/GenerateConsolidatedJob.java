package com.cmc.bkpclient.job;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.cmc.bkpclient.job.impl.GenerateConsolidatedJobImpl;

public class GenerateConsolidatedJob implements Job {

	private static final Logger logger = LogManager.getLogger(GenerateConsolidatedJob.class);

	public GenerateConsolidatedJob() {
	}

	public void execute(JobExecutionContext context) throws JobExecutionException {

		Timestamp tstInicio = new Timestamp(new Date().getTime());
		logger.info("****" + " Inicia Ejecucion Job: GenerateConsolidatedJob - " + tstInicio + "****");
		GenerateConsolidatedJobImpl generateConsolidatedJobImpl = null;
		
		try {
				generateConsolidatedJobImpl = new GenerateConsolidatedJobImpl();
				generateConsolidatedJobImpl.execute();

		} catch (IOException e) {
			
			logger.error("****" + " Error Ejecucion Job: GenerateConsolidatedJob " + "****" + e.getLocalizedMessage());
		
		} finally {
			
			Timestamp tstFin = new Timestamp(new Date().getTime());
			logger.info("****" + " Termina Ejecucion Job: GenerateConsolidatedJob - " + tstFin + "****");
		}
	}
}
