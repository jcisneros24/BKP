<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//Dth HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dth">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>BKP</title>
<!-- Bootstrap -->
<link href="css/bootstrap.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/sweetalert2.min.css">
<link rel="stylesheet" type="text/css" href="css/angular-cron-jobs.css">
<link href="css/bootstrap-switch.css" rel="stylesheet">
<link href="css/general-sfp.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/personalizar.css">
<link rel="stylesheet" type="text/css" href="css/ng-table.min.css">
</head>
<body data-ng-app="BKP">
	<div class="container" data-ng-controller="PanelControlador">
		<!-- ENCABEZADO -->
		<nav class="navbar navbar-default container">
		<div class="container-fluid">
			<div class="navbar-header">
				<a href="#"> <img class="img-responsive"
					src="images/logoBBVA.png">
				</a>
			</div>
			<ul class="nav navbar-nav navbar-right vcenter">
				<li>
					<h3 class="text-right vcenter">Panel de Configuración Backups</h3>
				</li>
			</ul>
		</div>
		</nav>

		<!-- CONFIGURACION CRON -->
		<div class="row">
			<div class="panel panel-primary filterable">
				<div class=" panelColor panel-heading ">
					<h3 class="panel-title">Configuración de Ejecuciones</h3>
					<div class="pull-right"></div>
				</div>
				<div>
					<table class="table">
						<thead>
							<tr>
								<th>#</th>
								<th>Descripción</th>
								<th>Meses</th>
								<th>Día de Semana</th>
								<th>Días</th>
								<th>Horas</th>
								<th>Minutos</th>
								<th>Segundos</th>
								<th>Estado</th>
								<th>Acción a Realizar</th>
							</tr>
						</thead>
						<tbody>
							<tr data-ng-repeat="data in lstcronDTO">
								<td>{{data.numero}}</td>
								<td>{{data.descripcion}}</td>
								<td>{{data.meses}}</td>
								<td>{{data.dia_semana}}</td>
								<td>{{data.dias}}</td>
								<td>{{data.horas}}</td>
								<td>{{data.minutos}}</td>
								<td>{{data.segundos}}</td>
								<td>{{data.v_estado}}</td>
								<td scope="row">
									<p data-placement="top" data-toggle="tooltip" title="Edit">
										<button id="btnEditar" class="btn btn-info btn-xs btn-ttc"
											data-title="Edit" data-toggle="modal"
											data-ng-click="cargarModalEdicion(data)">
											Editar <span class="glyphicon glyphicon-pencil"></span>
										</button>
									</p>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<!-- CONFIGURACION CARPETAS -->
		<div class="row" data-ng-controller="PanelControladorFolder">
			<div class="panel panel-primary filterable ">
				<div class=" panelColor panel-heading ">
					<h3 class="panel-title">Configuración de Rutas</h3>
					<div class="pull-right"></div>
				</div>
				<table id="dataTable" class="table">
					<thead>
						<tr>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th>
								<div class="pull-right">
									<button id="btnCreate" class="btn btn-xs btn-ttc-fucsia"
										data-title="Crear" data-toggle="modal"
										data-ng-click="cargarModalPathCreate()">
										Agregar Nueva Ruta <span
											class="glyphicon glyphicon-plus glyphicon"></span>
									</button>
								</div>
							</th>
						</tr>
					</thead>

				</table>
				<div>
					<table id="dataTable" class="table">
						<thead>
							<tr>
								<th>#</th>
								<th>Código Ruta</th>
								<th>Ruta de Archivo</th>
								<th align="left">Acción a Realizar</th>
							</tr>
						</thead>
						<tbody>
							<tr data-ng-repeat="data in pathsDTO">
								<td>{{data.id}}</td>
								<td>{{data.key}}</td>
								<td>{{data.value}}</td>
								<td align="left">
									<button class="btn btn-xs btn-ttc" data-title="Actualizar"
										data-toggle="modal"
										data-ng-click="cargarModalPathEdit(data.key)">
										Actualizar <span class="glyphicon glyphicon-pencil"></span>
									</button>
									<button class="btn btn-xs btn-ttc-fucsia" data-title="Eliminar"
										data-ng-click="btnEliminar(data.key)">
										<span class="glyphicon glyphicon-minus"></span> Eliminar
									</button>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<footer class="footer">
	<div class="container">
		<p class="text-muted text-center">BKP 1.0 - CMC</p>
	</div>
	</footer>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="js/jquery-3.1.1.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="js/bootstrap.min.js"></script>
	<script src="js/angular.js"></script>
	<script src="js/angular-cron-jobs.js"></script>
	<script src="js/angular-animate.min.js"></script>
	<script src="js/angular-touch.min.js"></script>
	<script src="js/ui-bootstrap-tpls-2.5.0.min.js"></script>
	<script src="js/aplicacion.js"></script>
	<script src="js/controladorBKP.js"></script>
	<script src="js/sweetalert2.min.js"></script>
	<script src="js/bootstrap-switch.js"></script>
	<script src="js/ng-table.min.js"></script>

</body>
</html>