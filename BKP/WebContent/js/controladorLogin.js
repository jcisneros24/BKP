/**
 * CONTROLADOR LOGIN -> ANGULAR - CMC
 */

angular.module("login", [ 'ngCookies' ]).controller(
		'LoginControlador',
		function($scope, $log, $http, $window, $cookies) {

			$scope.userBean = {};

			$scope.btnLogin = function() {

				$scope.user = {
					user : $scope.userBean.user,
					password : $scope.userBean.password
				}

				$http({
					method : "PUT",
					data : $scope.user,
					url : "/BKP/controller/methods/validarLogin"
				}).then(
						function mySucces(response) {

							if (response.data.respuesta == "ERROR") {
								swal(response.data.respuesta,
										response.data.data, "error");
							} else {
								if (response.data.data == "Administrador"
										|| response.data.data == "Usuario") {
									var expireDate = new Date();
									expireDate.setDate(expireDate.getDate() + 0.33);
									console.log("Session expire date: " + expireDate);

									$cookies.put("user",
											response.data.respuesta, {path:'/'}, {
												'expires' : expireDate
											});
									$cookies.put("perfil", response.data.data, {path:'/'},
											{
												'expires' : expireDate
											});
									$window.location.href = 'index.jsp';

								} else {
									swal(response.data.respuesta,
											response.data.data, "error");
								}
							}
						},
						function myError(response) {
							swal(response.data.respuesta, response.data.data,
									"error");
						});

			}

			// recognize 'Enter' press on form
			$(document).ready(function() {
				$('#password').keypress(function(e) {
					if (e.keyCode == 13)
						$('#submit').click();
				});
				$('#user').keypress(function(e) {
					if (e.keyCode == 13)
						$('#submit').click();
				});
			});

		});