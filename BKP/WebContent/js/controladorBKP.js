/**
 * CONTROLADOR MAESTRO -> ANGULAR - BKP
 */
/** *********************** PanelControlador ************************ */
app.controller('PanelControlador', function($scope, $log, $http, $uibModal,
		NgTableParams) {

	$scope.cargarModalEdicion = function(cronDTO) {

		$scope.cronEdit = {
			numero : cronDTO.numero,
			descripcion : cronDTO.descripcion,
			meses : cronDTO.meses,
			dia_semana : cronDTO.dia_semana,
			dias : cronDTO.dias,
			horas : cronDTO.horas,
			minutos : cronDTO.minutos,
			segundos : cronDTO.segundos,
			estado : cronDTO.estado,
			expresion : cronDTO.expresion,
			ruta : cronDTO.ruta
		};
		
		$uibModal.open({
			templateUrl : 'modal_cron.html',
			controller : 'ModalControlador',
			scope : $scope,
			resolve : {
				devolverCronDTO : function() {
					return $scope.cronEdit;
				}
			}
		});

	};

	$scope.refresh = function() {

		$http({
			method : "GET",
			url : "/BKP/controller/methods/obtenerCRON"
		}).then(function mySucces(response) {

			$scope.lstcronDTO = response.data;

			// Parsear valores de cron a Literal
			angular.forEach($scope.lstcronDTO, function(value, key) {
				$scope.convertirValoresCronALiteral(value);
			});

		}, function myError(response) {
			$scope.error = response.statusText;
		});

	};

	$scope.convertirValoresCronALiteral = function(cronDTO) {
		
		/** CONFIGURACION DE SEMANA **/
		
		switch (cronDTO.dia_semana) {
		case "?":
			cronDTO.v_dia_semana = "Todos";
			break;
		case "0":
			cronDTO.v_dia_semana = "Domingo";
			break;
		case "1":
			cronDTO.v_dia_semana = "Lunes";
			break;
		case "2":
			cronDTO.v_dia_semana = "Martes";
			break;
		case "3":
			cronDTO.v_dia_semana = "Miercoles";
			break;
		case "4":
			cronDTO.v_dia_semana = "Jueves";
			break;
		case "5":
			cronDTO.v_dia_semana = "Viernes";
			break;
		case "6":
			cronDTO.v_dia_semana = "Sábado";
			break;
		}
				
		/** CONFIGURACION DE ESTADO **/
		
		if (cronDTO.estado === "A") {
			
			cronDTO.v_estado = "Activo";
			
		} else if (cronDTO.estado === "I") {
			
			cronDTO.v_estado = "Inactivo";
		}

	};

	$scope.refresh();

});

/** *********************** PanelControladorFolder ************************ */

app.controller('PanelControladorFolder',function($scope, $log, $http, $uibModal) {

		$scope.cargarModalPathCreate = function() {

			$uibModal.open({
				templateUrl : 'modal_pathCreate.html',
				controller : 'ModalControlRuta',
				scope : $scope,
				resolve : {
					devolverFolderBean : function() {
						return $scope;
					}
				}
			});

		};

		$scope.refreshFolders = function() {
			$http({
				method : "GET",
				url : "/BKP/controller/methods/obtenerLstPaths"
			}).then(function mySucces(response) {

				$scope.pathsDTO = response.data;
				$log.info($scope.pathsDTO);

			}, function myError(response) {
				$scope.error = response.statusText;
			});

		};

		$scope.cargarModalPathEdit = function(key) {
			
			$http({
				method : "GET",
				params : {
					llave : key,
				},
				url : "/BKP/controller/methods/obtenerPath"
			}).then(function mySucces(response) {

				$scope.editPathDTO = response.data;

			}, function myError(response) {
				$scope.error = response.statusText;
			});
			

			$uibModal.open({
				templateUrl : 'modal_pathEdit.html',
				controller : 'ModalControlRuta',
				scope : $scope,
				resolve : {
					devolverFolderBean : function() {
						return $scope;
					},
					folderPathDTO : function() {
						return $scope.editPathDTO;
					}
				}
			});

		};

		$scope.pathDTO = {
			llave : "",
			valor : ""
		};

		$scope.pathsDTO = [];

		$scope.initVar = function(folderBean) {

			$scope.pathEdit = {
				codigo : folderBean.codigo,
				direccion : folderBean.direccion
			};
		};

		/***Funcion Eliminar Directorio***/
		
		$scope.btnEliminar = function(key) {

			swal({
				title : "¿Estás seguro?",
				text : "No se podrá recuperar la ruta eliminada",
				type : "warning",
				showCancelButton : true,
				confirmButtonColor : "#F6891E",
				confirmButtonText : "Aceptar",
				cancelButtonText : "Cancelar",
				reverseButtons: false
			}).then (function() {

				deleteFolder(key);

			});

		};

		function deleteFolder(key) {

			$http({
					method : "DELETE",
					url : "/BKP/controller/methods/eliminarPropiedad",
					params : {
						llave : key
					}
			}).then(function mySucces(response) {

						$scope.refreshFolders();
						swal(response.data.respuesta, response.data.data, "error");

			});
		};
		
		$scope.refreshFolders();

	});

/** *********************** ModalControlador ************************ */
app.controller('ModalControlador', function($scope, $log, $http,
		devolverCronDTO, $uibModalInstance) {

	$(document).ready(
			function() {
				
				$('[name="estado"]').bootstrapSwitch();
				
				if (devolverCronDTO.estado === "A") {
					
					$('input[name="estado"]').bootstrapSwitch('state', true);
					
				}
					$('input[name="estado"]').on('switchChange.bootstrapSwitch',
						
						function(event,state) {
						
							if (state) {

								$scope.cronEdit.estado = "A";
								
							} else {
								$scope.cronEdit.estado = "I";
							}

						});

			});

	$scope.cronEdit = {
		numero : "",
		descripcion : devolverCronDTO.descripcion,
		meses : "",
		dia_semana : "",
		dias : "",
		horas : "",
		minutos : "",
		segundos : "",
		estado : devolverCronDTO.estado,
		expresion : devolverCronDTO.expresion,

	};

	$scope.cronEdit = devolverCronDTO;

	$scope.cancel = function() {
		$uibModalInstance.dismiss('cancel');
	};

	$scope.btnEdit = function() {

		$http({
			method : "PUT",
			headers : {
				'Content-Type' : 'application/json'
			},
			data : $scope.cronEdit,
			url : "/BKP/controller/methods/actualizarCron"
		}).then(function mySucces(response) {

			$scope.refresh();

			swal(response.data.respuesta, response.data.data, "success");
			$uibModalInstance.dismiss('cancel');

		}, function myError(response) {

			swal(response.data.respuesta, response.data.data, "error");
		});

	}
});

/** *********************** ModalControlRuta ************************ */
app.controller('ModalControlRuta', function($scope, $log, $http,
		$uibModalInstance) {
	
	$scope.folderDTO = {};
	$scope.folderBean = {

		key : "",
		value : ""
	}

	$scope.cancel = function() {
		$uibModalInstance.dismiss('cancel');
	};
	
	/***Funcion Crear Directorio***/
	
	$scope.btnCreate = function() {

		$scope.folderBean = {

			key : $scope.folderDTO.llave,
			value : $scope.folderDTO.valor
		}

		console.log($scope.folderBean);

		$http({
			method : "POST",
			headers : {
				'Content-Type' : 'application/json'
			},
			data : $scope.folderBean,
			url : "/BKP/controller/methods/guardarPropiedad"

		}).then(function mySucces(response) {
			
			if (response.data.respuesta == "ERROR") {
				swal(response.data.respuesta, response.data.data, "error");
				} else {
					$scope.refreshFolders();
					swal(response.data.respuesta, response.data.data, "success");
					$uibModalInstance.dismiss('cancel');
				}
		
		}, function myError(response) {

			swal(response.data.respuesta, response.data.data, "error");	
			
		});

	}
	
	/***Funcion Editar Directorio***/
	
	$scope.btnEdit = function() {

		key = $scope.editPathDTO.key;
		value = $scope.editPathDTO.value;
		
		$log.info($scope.folderBean);
		
		$http({
			method : "PUT",
			params : {
				llave : key,
				valor : value
			},
			url : "/BKP/controller/methods/actualizarPropiedad"

		}).then(function mySucces(response) {

			if (response.data.respuesta == "ERROR") {
				swal(response.data.respuesta, response.data.data, "error");
				} else {
					$scope.refreshFolders();
					swal(response.data.respuesta, response.data.data, "success");
					$uibModalInstance.dismiss('cancel');
				}
			
		});

	};

});